@extends('layouts.app')

@section('content')
  <div class="page-body container network">

    {!! the_archive_description() !!}

    {!! do_shortcode('[leaflet-map height="440" lat=38.49767053694415 lng=-84.99256653551366 fitbounds zoomcontrol="true" doubleclickzoom="true" !tap]') !!}

    <h2 class="network__title d-none d-lg-block">Our Network</h2>
    <ul class="network__list">
      @while (have_posts()) @php the_post() @endphp
       <li>
         @include('partials.content-'.get_post_type())
       </li>
      @endwhile
    </ul>
  </div>

@endsection
