@extends('layouts.app')

@section('content')

<div class="page-body">
  {!! the_archive_description() !!}

  @if (have_posts())
  <div class="archive-squares">
    @php $index = 0; @endphp
    @while (have_posts()) @php the_post() @endphp

      @if (($index % 3) === 0)
        @if ($index > 1)
          </div>
        @endif
        <div class="row">
      @endif

      <div class="col-sm-12 col-md-4">
        @include('partials.content-'.get_post_type())
      </div>

      @php $index++; @endphp

    @endwhile
  </div>

    <div class="row">
      <div class="col-12">
        {!! bootstrap_pagination() !!}
      </div>
    </div>
  @endif
</div>

@endsection
