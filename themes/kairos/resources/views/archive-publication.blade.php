@extends('layouts.app')

@php
  $has_featured_image = true;
@endphp

@section('content')
  @include('partials.pressroom-cpt-archives', array('has_featured_image'=> true))
@endsection
