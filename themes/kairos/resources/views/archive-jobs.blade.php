@extends('layouts.app')

@section('content')

<div class="page-body">
  {!! the_archive_description() !!}

  @if (have_posts())

  <div class="jobs-list">
    @while (have_posts()) @php the_post() @endphp

      <article @php post_class() @endphp>
        <h3 class="entry-title"><a href="{{ get_permalink() }}">{!! get_the_title() !!}</a></h3>
        @include('partials/entry-meta')
      </article>

    @endwhile
  </div>

  @endif
</div>

@endsection
