@extends('layouts.app')

@section('filter')
  @if (is_home() || is_tax('category') || is_author())
    @include('partials.filter')
  @endif
@endsection

@section('content')
<div class="results">
  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Sorry, no results were found.', 'sage') }}
    </div>
    {!! get_search_form(false) !!}
  @endif

    @php $index = 0; @endphp
    @while (have_posts()) @php the_post() @endphp

      @if (($index % 2) === 0)
        @if ($index > 1)
          </div>
        @endif
        <div class="row">
      @endif

      <div class="col-xs-12 col-sm-6">
        @include('partials.content-'.get_post_type())
      </div>

      @php $index++; @endphp

    @endwhile

    @if (($index % 2) === 0 || $index == 1)
      @if ($index > 1 || $index == 1)
        </div>
      @endif
    @endif
    {{-- {!! get_the_posts_navigation() !!} --}}

  <div class="row">
    <div class="col-12">
      {!! bootstrap_pagination() !!}
    </div>
  </div>
  </div>
@endsection
