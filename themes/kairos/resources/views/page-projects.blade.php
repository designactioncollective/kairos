@extends('layouts.app')

@php
  $terms = get_terms('resource-project-name');
@endphp

@section('content')
  @while(have_posts()) @php the_post() @endphp
    <div class="page-body container">
      @php the_content() @endphp

      @php $index = 0; @endphp
      @foreach ($terms as $term)
        @php
          $image = get_field('featured_image', $term);
          $description = get_field('project_description', $term);
        @endphp

          @if (($index % 4) === 0)
            @if ($index > 1)
              </div>
            @endif
            <div class="row">
          @endif

          <div class="col-12 col-sm-6 col-lg-3">
            <div class="project campaigns">
              <a class="project__title project__title--small campaigns__title" href="/projects/{!! $term->slug !!}">
                <h2 class="project__title project__title--small campaigns__title">
                  {!! $term->name !!}
                </h2>
              </a>

              <div class="project__decorative-line campaigns__decorative-line"></div>
              <div class="image-container project__image-container project__image-container--small">
                <img
                class="project__image"
                srcset="
                {!! $image['sizes']['thumbnail'] !!} {!! $image['sizes']['thumbnail-width'] !!}w,
                {!! $image['sizes']['medium'] !!} {!! $image['sizes']['medium-width'] !!}w
                "
                src="{{ the_post_thumbnail_url('medium') }}"
                >
              </div>
            </div>
          </div>

          @php $index++; @endphp

      @endforeach
      {{-- Get Taxonomy project-name, name, slug, feautred image, project description --}}
      {{-- @include('partials.content-submenu') --}}
    </div>
  @endwhile
@endsection
