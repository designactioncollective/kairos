@extends('layouts.app')

@section('content')

  <div class="page-body">
    {!! the_archive_description() !!}

    @if (have_posts())
    <div class="archive-card-container">
      @php $index = 0; @endphp
      @while (have_posts()) @php the_post() @endphp

        @if (($index % 3) === 0)
          @if ($index > 1)
            </div>
          @endif
          <div class="row">
        @endif

        <div class="col-sm col-md-4">
          @include('partials.content-'.get_post_type())
        </div>

        @php $index++; @endphp

      @endwhile
    </div>
    @endif
  </div>
@endsection
