@extends('layouts.app')

@php
  // get the current taxonomy term
  $term = get_queried_object();

  // print_r(get_query_var( 'term' ));
  // print_r($term);

  $args = array(
    'post_type' => 'resources_cpt',
    'posts_per_page' => 2,
    'tax_query' => array(
      'relation' => 'AND',
      array(
        'taxonomy'         => 'resource-project-name',
        'terms'            => $term->slug,
        'field'            => 'slug',
        'operator'         => 'IN',
        'include_children' => true,
      ),
    ),
  );

  $the_query = new WP_Query( $args );

@endphp

@section('content')
<div class="page-body">
  {!! get_field('project_description', $term) !!}
</div>
@endsection


@section('main-footer')
<div class="taxonomy-home__home">
  <h2 class="taxonomy-home__title">
    {!! $term->name !!} Resources
  </h2>

  <div class="row">
    @while ($the_query->have_posts()) @php $the_query->the_post() @endphp
      <div class="col-12 col-md-6">
        @include('partials.content-card--featured-resource', array('is_featured'=> true))
      </div>
    @endwhile
  </div>

  <div class="taxonomy-home__button-container">
    <a class="btn btn-primary taxonomy-home__view-all-btn" aria-label="View All {!! the_title() !!} Posts" href="/resources_cpt/?_sft_resource-project-name={!! get_query_var( 'term' ) !!}">View All</a>
  </div>
</div>
@endsection

@php wp_reset_postdata() @endphp
