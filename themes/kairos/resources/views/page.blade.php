@extends('layouts.app')

{{-- TODO: This probs can be a func, or be included in the wrapper somehow? --}}
{{-- @section('page-title')
  @while(have_posts()) @php the_post() @endphp
    <div class="page-head container-fluid no-row">
      @include('partials.page-header')
    </div>
  @endwhile
@endsection --}}

@section('content')
  @while(have_posts()) @php the_post() @endphp
    <div class="page-body container no-row">
      @include('partials.content-page')
    </div>
  @endwhile
@endsection
