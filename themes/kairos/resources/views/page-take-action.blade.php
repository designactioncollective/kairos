@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    <div class="page-body container no-row">
      {{ the_content() }}

      @php
      $locations = get_nav_menu_locations();
      $menu = wp_get_nav_menu_object( $locations[ 'primary_navigation' ] );
      $menu_items = wp_get_nav_menu_items($menu->term_id);

      $target_parent_menu_item_id = NULL;
      $subpages = array();

      foreach( $menu_items as $item ) {
        if( get_the_ID() == $item->object_id ) {
          $target_parent_menu_item_id = $item->ID;
        }
      }
      foreach( $menu_items as $item ) {
        if( $target_parent_menu_item_id == $item->menu_item_parent ) {
          array_push($subpages, $item);
        }
      }
      @endphp

      @php
        $featured_images_ta = get_field('featured_images_ta');
        $index = 0;
      @endphp

      @foreach ($subpages as $page)
      @php
        if ($featured_images_ta[$index] !== NULL) {
          $image = $featured_images_ta[$index]['image_ta']; // Image defaults to whatever the last image was
        }
      @endphp

        @if (($index % 4) === 0)
          @if ($index > 1)
            </div>
          @endif
          <div class="row">
        @endif

        <div class="col-12 col-sm-6 col-md-4">
          <div class="take-action project campaigns">
            <a class="project__title project__title--small campaigns__title" href="{!! $page->url !!}">
              <h2 class="project__title project__title--small campaigns__title">
                {!! $page->title !!}
              </h2>
            </a>

            <div class="project__decorative-line campaigns__decorative-line"></div>
            {{-- <div class="image-container project__image-container project__image-container--take-action">
              <img
              class="project__image"
              srcset="
              {!! $image['sizes']['thumbnail'] !!} {!! $image['sizes']['thumbnail-width'] !!}w,
              {!! $image['sizes']['medium'] !!} {!! $image['sizes']['medium-width'] !!}w
              "
              src="{{ the_post_thumbnail_url('medium') }}"
              >
            </div> --}}
            <div class="image-container project__image-container project__image-container--take-action">
              {{-- @php
                $large_photo = wp_get_attachment_image_src( get_post_thumbnail_id( ), 'take-action-lg')[0];
                $medium_photo = wp_get_attachment_image_src( get_post_thumbnail_id( ), 'take-action-md')[0];
                $small_photo = wp_get_attachment_image_src( get_post_thumbnail_id( ), 'take-action-sm')[0];
              @endphp --}}
              <img
                  class="project__image"
                  srcset="
                          {{ $image['sizes']['take-action-lg'] }} 1200w,
                          {{ $image['sizes']['take-action-md'] }} 769w,
                          {{ $image['sizes']['take-action-sm'] }} 0w,
                          "
                  src="{{ $image['sizes']['take-action-sm'] }}"
                />
            </div>
          </div>
        </div>

        @php $index++; @endphp

      @endforeach

    </div>
  @endwhile
@endsection
