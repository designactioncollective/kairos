@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
  {{-- <div class="page-body container no-row"> --}}
    <div class="page-body tempting">
    {{-- <div class="page-body container"> --}}
      {{-- <div class="row">
        <div class="col-12"> --}}
          @include('partials.content-single-'.get_post_type())
        {{-- </div>
      </div> --}}
    </div>
  @endwhile
@endsection
