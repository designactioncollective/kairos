@php
  $cats = wp_get_post_categories( get_the_ID() );
@endphp

@if (count($cats) > 0)
<a href="/updates/?_sft_category=category-{!! $cats[0] !!}">
  <span class="category">{!! $cats[0] !!}</span>
</a>
@endif
