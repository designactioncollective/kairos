<article @php post_class() @endphp>
  <div class="entry-content post">
    <header>
      <div class="container-fluid p-0">
        <div class="row">
          <div class="col-12 col-md-8">
            {!! the_post_thumbnail('large') !!}
          </div>
          <div class="col-12 col-md-4 post__details">
            @if (is_singular('post'))
              <div class="post__author">
                @include('partials.entry-meta-author-for-posts')
              </div>
            @endif
            <div class="post__date">
              @include('partials.entry-meta')
            </div>
            <div class="post__category">
              <span class="post__category__label">Type:</span>
              <a href="/op-eds/"><span class="category">Op-ed</span></a>
            </div>
          </div>
        </div>
      </div>
    </header>
    @php the_content() @endphp
  </div>
</article>
