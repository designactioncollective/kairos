@php
  $name = get_the_terms( get_the_ID(), 'resource-project-name' );
  $type = get_the_terms( get_the_ID(), 'resource-type' );
@endphp

{{-- This will only show one project-name, even if post has many --}}
@if ($cats !== false)
<div class="resource-details {!! $container_class !!}">
  <time class="updated" datetime="{{ get_post_time('c', true) }}">{{ get_the_date() }}</time>
    <div>
      <span class="resource-details__label">Related Project:</span> <span class="resource-details__name"><a href="/resources_cpt/?_sft_resource-project-name={!! $name[0]->slug !!}">{!! $name[0]->name !!}</a></span>
    </div>
    <div>
      <span class="resource-details__label">Type of Resource:</span> <span class="resource-details__type"> <a href="/resources_cpt/?_sft_resource-type={!! $type[0]->slug !!}">{!! $type[0]->name !!}</a></span>
    </div>
  </div>
@endif
