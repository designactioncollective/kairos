<header class="banner">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <nav class="nav-primary" aria-label="{{ get_bloginfo('name', 'display') }}">

          @if ( function_exists( 'the_custom_logo' ) )
            <div class="image-container nav-primary__left">
              {!! the_custom_logo() !!}
            </div>
          @else :
            <a class="brand nav-primary__left" href="{{ home_url('/') }}">{{ get_bloginfo('name', 'display') }}</a>
          @endif

        <div class="nav-primary__right d-none d-lg-flex">
          <div class="nav-primary__right__top utility-navigation">
            {!! do_shortcode('[gtranslate]') !!}
            <ul class="nav-primary__social-media nav-primary__list">
              <li>
                <a href="{!! get_theme_mod('facebook_link', '') !!}" target="_blank"><i class="fab fa-facebook"></i></a>
              </li>
              <li>
                <a href="https://twitter.com/{!! get_theme_mod('twitter_link', '') !!}" target="_blank"><i class="fab fa-twitter"></i></a>
              </li>
              <li>
                <a href="{!! get_theme_mod('instagram_link', '') !!}" target="_blank"><i class="fab fa-instagram"></i></a>
              </li>
            </ul>
            <ul class="nav-primary__list ut_nav">
              <li>
                <a href="{!! get_theme_mod('donation_link', '') !!}" class="btn btn-primary btn-sm btn__utility-navigation donation">{!! get_theme_mod('donation_title', '') !!}</a>
              </li>
              <li>
                <a href="{!! get_theme_mod('store_link', '') !!}" class="btn btn-primary btn-sm btn__utility-navigation donation">{!! get_theme_mod('store_title', '') !!}</a>
              </li>
              <li>
                <a href="{!! get_theme_mod('sign_up_link', '') !!}" class="btn btn-primary btn-sm btn__utility-navigation donation">{!! get_theme_mod('sign_up_title', '') !!}</a>
              </li>
            </ul>
            @include('partials.header-searchform')
          </div>
          @include('partials.primary_navigation', array('ulClass' => 'nav-primary__right__bottom'))
        </div>

        <div class="nav-primary__bottom d-lg-none">
          @include('partials.primary_navigation', array('ulId' => 'flyout-menu', 'ulClass' => 'nav-primary__bottom__bottom'))
          <div class="nav-primary__bottom__top utility-navigation">
            <ul class="nav-primary__list">
              <li>
                <a href="{!! get_theme_mod('donation_link', '') !!}" class="btn btn-primary btn-sm btn__utility-navigation donation">{!! get_theme_mod('donation_title', '') !!}</a>
              </li>
              <li>
                <a href="{!! get_theme_mod('store_link', '') !!}" class="btn btn-primary btn-sm btn__utility-navigation donation">{!! get_theme_mod('store_title', '') !!}</a>
              </li>
              <li>
                <a href="{!! get_theme_mod('sign_up_link', '') !!}" class="btn btn-primary btn-sm btn__utility-navigation donation">{!! get_theme_mod('sign_up_title', '') !!}</a>
              </li>
            </ul>
            <div class="flyout-menu-toggler">
              <span id="hamburger-icon" aria-hidden="true"></span>
              <button id="menu-toggle" name="menu-toggle" aria-haspopup="true" aria-expanded="false" aria-controls="flyout-menu" class="visually-hidden">
              Menu
              </button>
            </div>
            <div class="nav-primary__utilities d-none d-md-flex">
              <ul class="nav-primary__social-media nav-primary__list">
                <li>
                  <a href="{!! get_theme_mod('facebook_link', '') !!}" target="_blank"><i class="fab fa-facebook"></i></a>
                </li>
                <li>
                  <a href="https://twitter.com/{!! get_theme_mod('twitter_link', '') !!}" target="_blank"><i class="fab fa-twitter"></i></a>
                </li>
                <li>
                  <a href="{!! get_theme_mod('instagram_link', '') !!}" target="_blank"><i class="fab fa-instagram"></i></a>
                </li>
              </ul>
              {!! do_shortcode('[gtranslate]') !!}
              @include('partials.header-searchform')
            </div>
          </div>
        </div>
        </nav>
      </div>

      </div>
    </div>
</header>
