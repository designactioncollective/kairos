<div id="{!! $ulId !!}">
  <div class="scrollbar">
    <div class="nav-primary__utilities d-md-none">
      <ul class="nav-primary__social-media nav-primary__list">
        <li>
          <a href="{!! get_theme_mod('facebook_link', '') !!}" target="_blank"><i class="fab fa-facebook"></i></a>
        </li>
        <li>
          <a href="{!! get_theme_mod('twitter_link', '') !!}" target="_blank"><i class="fab fa-twitter"></i></a>
        </li>
        <li>
          <a href="{!! get_theme_mod('instagram_link', '') !!}" target="_blank"><i class="fab fa-instagram"></i></a>
        </li>
      </ul>
      {!! do_shortcode('[gtranslate]') !!}
      @include('partials.header-searchform')
    </div>
    <ul
        class="a11y-menu disclosure-nav {!! $ulClass !!}">
    @php
      // TODO: This belongs in a controller. You should just be able to use the same data to output whatever template you want.
      $menu_name = 'primary_navigation'; // ATTENTION: REPLACE "primary_navigation" WITH THE LOCATION NAME OF YOUR MENU.
      $is_first_link = true;
      if ( ( $locations = get_nav_menu_locations() ) && isset( $locations[ $menu_name ] ) ) {
          $menu = wp_get_nav_menu_object( $locations[ $menu_name ] );

          $menu_items = wp_get_nav_menu_items($menu->term_id);
          $menu_list = '';
          $count = 0;
          $submenu = false;
          $cpi = get_the_id();

          foreach( $menu_items as $current ) {
              if($cpi == $current->object_id ) {
                if ( !$current->menu_item_parent ) {
                  $cpi = $current->ID;
                } else {
                $cpi = $current->menu_item_parent;
              }
              $cai = $current->ID;
              break;
            }
          }

          foreach( $menu_items as $menu_item ) {
              $link = $menu_item->url;
              $title = $menu_item->title;
              $menu_id = $menu_item->post_name;
              $menu_item->ID == $cai ? $ac2=' current_menu' : $ac2='';
              $is_first_link = false;

              // Generate the li for this
              if ( !$menu_item->menu_item_parent ) {
                $parent_id = $menu_item->ID;
                $parent_id == $cpi ? $ac=' current_item' : $ac='';

                if (!empty($menu_items[$count + 1]) && $menu_items[ $count + 1 ]->menu_item_parent == $parent_id ) { //Checking has child
                      $submenu_aria_label_title = $title;
                      $menu_list .= '
                      <li
                        class="'.$ac.'"
                      >
                        <div class="disclosure-nav__link-box">
                          <a  href="'.$link.'"
                              class="'.$ac2.'"
                              href="#">'.$title.'</a>';

                        $menu_list .= '
                          <button type="button"
                              aria-expanded="false"
                              aria-controls="menu_'.$menu_id.'" aria-label="Expand '.$title.' Sub-Menu">
                              <i class="menu_arrow fal fa-chevron-down"></i>
                          </button>
                        </div>
                      ' ."\n";
                  } else {
                      $menu_list .= '
                      <li
                        class="'.$ac.'">' ."\n";
                      $menu_list .= '<a
                                      href="'.$link.'"
                                      class="'.$ac2.'">'.$title.'</a>' ."\n";
                  }
              }
              if ( $parent_id == $menu_item->menu_item_parent ) {
                  if ( !$submenu ) {
                      $submenu = true;
                      $menu_list .= '<ul
                                      style="display:none;visibility:hidden;"
                                      id="menu_'.$menu_id.'"
                                      class="sub-menu"
                                      aria-label="'.$submenu_aria_label_title.' Sub-Menu">' ."\n";
                  }
                  $menu_list .= '<li>' ."\n";
                  $menu_list .= '<a
                                  href="'.$link.'"
                                  class="'.$ac2.'">'.$title.'</a>' ."\n";
                  $menu_list .= '</li>' ."\n";
                  if (empty($menu_items[$count + 1]) || $menu_items[ $count + 1 ]->menu_item_parent != $parent_id && $submenu){
                      $menu_list .= '</ul>' ."\n";
                      $submenu = false;
                  }
              }
              if (empty($menu_items[$count + 1]) || $menu_items[ $count + 1 ]->menu_item_parent != $parent_id ) {
                  $menu_list .= '</li>' ."\n";
                  $submenu = false;
              }
              $count++;
          }
      } else {
          $menu_list = '<li>Menu "' . $menu_name . '" not defined.</li>';
      }

      echo $menu_list;
    @endphp
    </ul>
  </div>
</div>
