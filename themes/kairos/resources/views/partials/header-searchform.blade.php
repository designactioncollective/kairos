<div class="dropdown dropdown--search">
  <button class="btn" type="button" id="searchformDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <i class="far fa-search"></i>
  </button>
  <div class="dropdown-menu" aria-labelledby="searchformDropdown">
    {!! get_search_form() !!}
  </div>
</div>
