<footer class="content-info" role="contentinfo">
  <div class="container no-row container-min-width-100 container-min-width-100--until-lg">
    {{-- <nav> --}}
    {{-- </nav> --}}
    {{-- <div class="footer-menu"> --}}
    {{-- <nav> --}}
    {{-- </nav> --}}

    <nav aria-label="Footer">
      @php
        $args = array(
          'theme_location' => 'primary_navigation',
          'menu_class' => 'content-info__menu',
          'depth' => 1,
        );
        wp_nav_menu( $args );
      @endphp

      <ul class="content-info__social-media">
        <li>
          <a href="{!! get_theme_mod('facebook_link', '') !!}" target="_blank"><i class="fab fa-facebook fa-3x"></i></a>
        </li>
        <li>
          <a href="https://twitter.com/{!! get_theme_mod('twitter_link', '') !!}" target="_blank"><i class="fab fa-twitter fa-3x"></i></a>
        </li>
        <li>
          <a href="{!! get_theme_mod('instagram_link', '') !!}" target="_blank"><i class="fab fa-instagram fa-3x"></i></a>
        </li>
      </ul>

      <small>
        @if (has_nav_menu('footer_menu'))
        {!! wp_nav_menu(['theme_location' => 'footer_menu', 'menu_class' => 'content-info__small-menu']) !!}
        @endif
      </small>
    </nav>

    <small class="info">Copyright &copy; <?php echo date("Y"); ?> {{ __('Kairos', 'kairos') }} &ndash; All Rights Reserved</small>
    {{-- </div> --}}
  </div>
</footer>
