<article @php post_class() @endphp>
  <div class="card card--featured-resource">
    @include('partials/entry-meta-project-type')
    <div class="image-container card--featured-resource__image-container">
      {{ $is_featured ? the_post_thumbnail('large') : the_post_thumbnail('medium') }}
    </div>
    {{-- <div>
      <header> --}}
        <a class="card-link" href="{{ get_permalink() }}"><h3 class="card--featured-resource__title">{!! get_the_title() !!}</h3></a>
      {{-- </header>
    </div> --}}
  </div>
</article>
