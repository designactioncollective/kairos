@php
  $cats = wp_get_post_categories( get_the_ID() );
@endphp

@if (count($cats) > 0)
@php
    $term = get_term($cats[0], 'category');
@endphp
<a href="/updates/?_sft_category={!! $term->slug !!}">
  <span class="category">{!! $term->name !!}</span>
</a>
@endif
