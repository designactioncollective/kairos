<article @php post_class() @endphp>
  <div class="card card--category {!! $is_feature ? 'card--category-feature' : '' !!}">
    @if ($is_feature)
      {{-- The first, the large one --}}
      <header>
        @include('partials/entry-meta-categories-for-posts')
        <h3 class="entry-title"><a href="{{ get_permalink() }}">{!! get_the_title() !!}</a></h3>
        @include('partials/entry-meta')
      </header>
      <div>
        {!! the_excerpt() !!}
      </div>

    @else

      {{-- Subsequent little ones --}}
      <header>
        @include('partials/entry-meta-categories-for-posts')
        @include('partials/entry-meta')
        <h3 class="entry-title"><a href="{{ get_permalink() }}">{!! get_the_title() !!}</a></h3>
      </header>
    @endif

    <div class="author-details">
      @include('partials/entry-meta-author')
    </div>
  </div>
</article>
