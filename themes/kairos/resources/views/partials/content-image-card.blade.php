<article @php post_class('image-card') @endphp>
  <div class="image-container image-card__image-container">
    <img src="{{ the_post_thumbnail_url('resource-card') }}" alt="">
    <div class="img-gradient"></div>
  </div>
  <header class="image-card__body">
    @include($entryMetaPath , array('container_class'=> 'image-card__category-links'))
    <a href="{{ get_permalink() }}" class="image-card__main-link"><h2 class="entry-title image-card__entry-title">{!! get_the_title() !!}</h2></a>
  </header>
</article>
