@php
  $cats = wp_get_post_categories( get_the_ID() );
@endphp

{{-- This will only show one category, even if post has many --}}
@if (count($cats) > 0)
{{-- <span>{!! get_the_archive_title() !!} / {!! $cats[0] !!}</span> --}}
  <span class="category">{!! $cats[0] !!}</span>
@endif
