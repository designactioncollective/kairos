@php the_content() @endphp

@php
  // 64
  $use_this_key = NULL;
  $sub_menu_root_object_id = NULL;
  // if (is_single()) {
    $sub_menu_root_id = 64;
  // } elseif ( is_page_template('views/template-volunteer-opportunity.blade.php') ) {
    // $sub_menu_root_object_id = $post->post_parent;
  // }
@endphp
@php
    $args = array(
      'menu_class' => 'menu--child-pages',
      'show_parent' => false,
      'sub_menu_root_id'    => $sub_menu_root_id,
      // 'sub_menu_root_object_id'    => $sub_menu_root_object_id,
);
@endphp

{!! display_the_submenu($args) !!}
