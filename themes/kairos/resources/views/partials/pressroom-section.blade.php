<div class="archive-preview">
  <div class="container">
    <header class="archive-preview__header">
      <h2 class="archive-preview__title">
        {!! $title !!}
      </h2>
      <a href="{!! get_post_type_archive_link($slug) !!}" class="archive-preview__view-all-btn btn btn-primary">View All</a>
    </header>
    @php
      $args = array(
                'post_type' => $slug,
                'posts_per_page' => 4,
              );
      $the_query = new WP_Query( $args );
    @endphp

    @php $index = ($has_featured_image) ? -1 : 0; @endphp

    @if ( $the_query->have_posts() )
      @while ( $the_query->have_posts() ) @php $the_query->the_post() @endphp

        @if ($index == -1)
        <div class="row">
          <div class="col-sm">
            @include('partials.content-card--image-left', array('include_excerpt'=> true))
            {{-- <hr /> --}}
          </div>
        </div>
        @else

        @if (($index % 3) === 0)
          @if ($index > 1)
            </div>
          @endif
          <div class="row d-none d-lg-flex">
        @endif

        <div class="col-sm col-md-4 pressroom-border">
          @include('partials.content-card--image-left', array('include_excerpt'=> false))
        </div>

        @endif

        @php $index++; @endphp
      @endwhile
    </div>
    @else
        {{-- // no posts found --}}
    @endif

    @php wp_reset_postdata() @endphp
  </div>
</div>
