@php
$args = array(
  'post_type'      => "resources_cpt",
  'posts_per_page' => 5,
  'order'          => 'DESC',
);
$loop = new WP_Query( $args );

@endphp

@if ( $loop->have_posts() )
<section class="featured-resources section">
  <h2 class="section__title">{!! get_field('featured_resources_title') !!}</h2>

  <div class="archive-squares">
    <div class="container">
    @php
      $has_featured_image = true;
    @endphp
    @php $index = ($has_featured_image) ? -2 : 0; @endphp
    @while ( $loop->have_posts() )
      @php $loop->the_post() @endphp

      @if ($index < 0)

        {{-- Print first .row --}}
        @if ($index == -2)
          <div class="row">
        @endif

        <div class="col-sm col-md-6">
          @include('partials.content-card--featured-resource', array('is_featured'=> true))
        </div>

        {{-- Closes .row  --}}
        @if ($index == -1)
          </div> {{-- /.row --}}
        @endif
      @endif

      {{-- Second row --}}
      @if ($index >= 0)

        @if ($index == 0)
          <div class="row">
        @endif

        <div class="col-sm col-md-4">
          @include('partials.content-card--featured-resource', array('is_featured'=> false))
        </div>

        @if ($index == -2)
          </div>
        @endif
      @endif

      @php $index++; @endphp

      @endwhile
    </div> {{-- Closes the last .row --}}
    <div class="row">
      <div class="col-12 btn-container--center">
        <a href="{!! get_post_type_archive_link('resources_cpt') !!}" class="btn btn-primary btn-view-all-resources">View All</a>
      </div>
    </div>
    </div> {{-- Closes .container --}}
  </div> {{-- Closes .archive-square --}}


</section>
@endif
@php wp_reset_postdata(); @endphp
