@if( have_rows('take_action_section') )
  @while( have_rows('take_action_section') ) @php the_row() @endphp

  <section class="take-action-section section section--red">
    <div class="section--red-background"></div>
    <h2 class="section__title">{!! get_sub_field('title') !!}</h2>

    <section class="sign-up">
      <div class="container">
        <div class="row">
          <div class="col-sm">
            <div class="sign-up-form signup--home">
              {{-- <h2 class="sign-up-form__title">Sign up for updates</h2> --}}
              {{-- {!! do_shortcode('[wpforms id="224" title="false" description="false"]') !!} --}}
              {{-- <script src='https://actionnetwork.org/widgets/v3/form/sign-up-for-updates-123?format=js&source=widget'></script><div id='can-form-area-sign-up-for-updates-123' style='width: 100%'><!-- this div is the target for our HTML insertion --></div> --}}
              {!! get_sub_field('action_network_form') !!}
            </div>
          </div>
        </div>
      </div>
    </section>

    @if( have_rows('cards'))
      <section class="get-involved">
        <div class="container">

          <div class="row">
            @while( have_rows('cards') )
            @php the_row() @endphp

              @php
                $title = get_sub_field('title');
                $blurb = get_sub_field('blurb');
                $link = get_sub_field('link');
              @endphp

              <div class="col-sm col-md-4">
                <div class="card card--take-action">
                  <div class="card--take-action__body">
                    <h3 class="card--take-action__title">{{{ $title }}}</h3>
                    <p class="card--take-action__text">{{{ $blurb }}}</p>

                    @if( $link )
                    @php
                      $link_url = $link['url'];
                      $link_title = $link['title'];
                      $link_target = $link['target'] ? $link['target'] : '_self';
                    @endphp
                      <a href="{!! $link_url !!}" title="{!! $link_title !!}" class="btn btn-primary card--take-action__button" target="{{{ $link_target }}}">{{{ $link_title }}}</a>
                    @endif
                  </div>
                </div>
              </div>
            @endwhile
          </div>
        </div>

      </section>
    @endif

  </section>

  @endwhile
@endif
