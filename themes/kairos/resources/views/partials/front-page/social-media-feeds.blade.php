{{-- Social feeds --}}
@php
  // $facebook = get_field('facebook_page_id');
  // $twitter = get_field('twitter_handle');
  // $instagram = get_field('instagram_username');
  // $facebook = '#';
  // $twitter = '#';
  // $instagram = '#';
  $facebook = get_theme_mod( 'facebook_link' );
  $twitter = get_theme_mod( 'twitter_link' );
  $instagram = get_theme_mod( 'instagram_link' );
@endphp
<section class="social-media section">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <hr class="social-media__hr">
        <h2 class="section__title social-media__title">Follow the Kairos Center on Social Media</h2>
      </div>
    </div>
  </div>

  <div class="alignfull chapter__social">
    <div class="container">
      <div class="row">
        {{-- Facebook --}}
        @if ($facebook)
          <div class="col-12 col-md-6 col-xl-4">
            <h3>
              <hr class="social-feed__hr">
              <a class="social-feed__link" href="{!! $facebook !!}">
                <span class="sr-only">Facebook</span>
                <i class="fab fa-2x fa-facebook"></i>
              </a>
            </h3>
            <div class="social-feed__feed">
              {!! do_shortcode('[custom-facebook-feed]') !!}
            </div>
          </div>
        @endif

        {{-- Twitter --}}
        @if ($twitter)
          <div class="col-12 col-md-6 col-xl-4">
            <h3>
              <hr class="social-feed__hr">
              <a class="social-feed__link" href="https://twitter.com/{!! $twitter !!}">
                <span class="sr-only">Twitter</span>
                <i class="fab fa-2x fa-twitter"></i>
              </a>
            </h3>
            <div class="social-feed__feed">
              {!! do_shortcode('[custom-twitter-feed screenname='.$twitter.']') !!}
            </div>
          </div>
        @endif

        {{-- Instagram --}}
        @if ($instagram)
          <div class="col-xl-4">
            <h3>
              <hr class="social-feed__hr">
              <a class="social-feed__link" href="{!! $instagram !!}">
                <span class="sr-only">Instagram</span>
                <i class="fab fa-2x fa-instagram"></i>
              </a>
            </h3>
            <div class="social-feed__feed social-feed__feed--instagram">
              {!! do_shortcode('[instagram-feed cols=2 num="6"]') !!}
            </div>
          </div>
        @endif
      </div>
    </div>
  </div>

</section>
