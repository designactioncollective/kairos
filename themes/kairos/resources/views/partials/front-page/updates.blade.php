@php
  $args = array(
    'post_type'      => "post",
    'posts_per_page' => 4,
    'order'          => 'DESC',
  );
  $loop = new WP_Query( $args );
@endphp

{{-- <div>
  @php
      print_r(get_field('updates')['featured_image']);
  @endphp
</div> --}}

  @if ( $loop->have_posts() )
  <section class="updates section">

    <div class="archive-squares">
        @php
          $has_featured_image = true;
        @endphp
        @php $index = ($has_featured_image) ? -1 : 0; @endphp
        @while ( $loop->have_posts() )
          @php $loop->the_post() @endphp

          @if ($index == -1)
          <div class="updates__row-one">
            <div class="section--red-background"></div>
            <div class="container">
              <div class="row">
                <div class="col-sm d-md-none updates__presentation-image-container">
                  <div class="updates__presentation-image-container__wrapper">
                    <img class="updates__presentation-image-container__wrapper__image" role="presentation" src="{!! get_field('updates', 75)['featured_image']['url'] !!}" alt="">
                  </div>
                </div>
                <div class="col-xs-12 col-md-6">
                  @include('partials.content-card--category', array('is_feature'=> true))
                </div>
                {{-- Second column for displaying background image --}}
                <div class="col-xs-12 col-md-6 d-none d-md-block">
                  <img class="updates__presentation-image" role="presentation" src="{!! get_field('updates', 75)['featured_image']['url'] !!}" alt="">
                </div>
              </div>
          @else

          @if (($index % 3) === 0)

            @if ($index === 0)
              </div>
            </div>
            <div class="updates__row-two d-none d-md-flex">
              <div class="section--red-background"></div>
              <div class="container">
              @endif

                @if ($index > 1)
                    </div>
                  </div>
                @endif
                <div class="row">
              @endif

            <div class="col-xs-12 col-lg-4 col-md-6 {{ ($index === 2) ? 'd-none d-lg-block' : '' }}">
              @include('partials.content-card--category', array('is_feature'=> false))
            </div>

            @if ($index === 2)
              </div>
            </div>
            @endif
          @endif

          @php $index++; @endphp

        @endwhile

      </div>
    </div>

  </section>
  @endif
  @php wp_reset_postdata(); @endphp
