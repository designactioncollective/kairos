<section class="slider section">
  @if( have_rows('homepage_slideshow') )
  {{-- <div class="container">
    <div class="row">
      <div class="col-12"> --}}

        <div class="owl-carousel">
          @while ( have_rows('homepage_slideshow') ) @php the_row(); @endphp
          <div class="slide">
            @php
              $slide_image = get_sub_field('slide_image');
              $slide_title = get_sub_field('slide_title');
              $link_type = get_sub_field('slide_link_type');
              if ($link_type == 'Internal') {
                $link = get_sub_field('internal_link');
              } else {
                $link = get_sub_field('external_link');
              }
              $slide_description = get_sub_field('slide_description');
            @endphp

            @if( $slide_image )
              <div class="slide__image">
                {!! wp_get_attachment_image( $slide_image, "full" ) !!}
              </div>
            @endif
            {{-- <div class="slide__body"> --}}
              <div class="slide__body container">
              <div class="row">
                <div class="col-12">
                  <div class="slide__title">
                    <a href="{{ $link }}">
                      <h3>{{ $slide_title }}</h3>
                    </a>
                  </div>
                  <div class="slide__description">
                    {{ $slide_description }}
                  </div>
                  <div class="slide__link">
                    <div class="btn btn-primary">Learn More</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          @endwhile
        </div>

      {{-- </div>
    </div>
  </div> --}}

  @endif
</section>
