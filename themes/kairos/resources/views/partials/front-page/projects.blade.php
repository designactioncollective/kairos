<section class="projects section">
  @php
      $args = array(
          'post_type'      => 'page',
          'posts_per_page' => -1,
          'post_parent'    => 25,
          'order'          => 'ASC',
          'orderby'        => 'menu_order'
      );
      $parent = new WP_Query( $args );
    @endphp

  @php
    $args = array(
      'taxonomy' => 'resource-project-name',
      'fields'  => 'id=>name'
  );
    $tax_list = get_categories( $args );
  @endphp
  <div class="container">

    <div class="row">
      <div class="col-12 section__title projects__title">{!! get_field('our_programs_title') !!}</div>
    </div>

    <div class="row">

      @foreach ($tax_list as $index => $item)
        <div class="col-6 col-md-3 project">

          <a class="project__title" href="{!! get_term_link( $index, $taxonomy = 'resource-project-name' ) !!}">
            <h3 class="project__title">
              {!! $item !!}
            </h3>
          </a>

          <div class="project__decorative-line decorative-line"></div>

          @php
            $image = get_field('featured_image', 'term_'.$index);
          @endphp

          <div class="image-container project__image-container">
            <img
            class="project__image"

            srcset="
            {!! $image['sizes']['thumbnail'] !!} {!! $image['sizes']['thumbnail-width'] !!}w,
            {!! $image['sizes']['medium'] !!} {!! $image['sizes']['medium-width'] !!}w
            "
            src="{!! $image['sizes']['medium'] !!}"
            >
          </div>
        </div>

      @endforeach
    </div>
  </div>

  @php wp_reset_postdata(); @endphp

</section>
