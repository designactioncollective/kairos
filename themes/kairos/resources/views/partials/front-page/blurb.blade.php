<section class="blurb section">
  <div class="container">
    <div class="row">
      <div class="col-12 blurb__wrapper">
        <p class="blurb__text">{{ get_field('blurb') }}</p>

        @php
          $link = get_field('blurb_button');
        @endphp
        @if( $link )
          @php
              $link_url = $link['url'];
              $link_title = $link['title'];
              $link_target = $link['target'] ? $link['target'] : '_self';
          @endphp
          <a class="blurb__btn btn btn-primary" href="{!! $link_url !!}" target="{!! $link_target !!}">{!! $link_title !!}</a>
        @endif
      </div>
    </div>
  </div>
</section>
