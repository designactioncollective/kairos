@php
  $name = get_the_terms( get_the_ID(), 'resource-project-name' );
  $type = get_the_terms( get_the_ID(), 'resource-type' );
@endphp

{{-- This will only show one project-name, even if post has many --}}
@if ($cats !== false)
  <div class="project-details {!! $container_class !!}">
    <div>
      <span class="project-details__name"><a href="/resources_cpt/?_sft_resource-project-name={!! $name[0]->slug !!}">{!! $name[0]->name !!}</a> / </span><span class="project-details__type"> <a href="/resources_cpt/?_sft_resource-type={!! $type[0]->slug !!}">{!! $type[0]->name !!}</a></span>
    </div>
  </div>
@endif
