<div class="page-header">
  <div class="container">
    @php if( dac_the_breadcrumbs('primary_navigation') ) dac_the_breadcrumbs('primary_navigation') @endphp
    <h1>{!! App::title() !!}</h1>
  </div>
</div>
