<span class="image-container">
  {!! get_avatar( get_the_author_meta('user_email'), $size = '50') !!}
</span>
<span class="author-name">{!! get_the_author_meta('display_name') !!}</span>
