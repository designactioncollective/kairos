<article @php post_class() @endphp>
  <div class="card card--square">
    {{-- <header>
      <h2 class="entry-title"><a href="{{ get_permalink() }}">{!! get_the_title() !!}</a></h2>
    </header>
    <div class="decorative-line"></div>
    <div class="image-container">
      {{ the_post_thumbnail('medium') }}
    </div> --}}

    <a class="project__title campaigns__title" href="{{ get_permalink() }}">
      <h2 class="project__title campaigns__title">
        {!! get_the_title() !!}
      </h2>
    </a>

    <div class="project__decorative-line campaigns__decorative-line"></div>

    @php
      $image = get_field('featured_image', 'term_'.$index);
    @endphp

    <div class="image-container project__image-container campaigns__image-container">
      <img
      class="project__image"

      {{-- srcset="
      {!! $image['sizes']['thumbnail'] !!} {!! $image['sizes']['thumbnail-width'] !!}w,
      {!! $image['sizes']['medium'] !!} {!! $image['sizes']['medium-width'] !!}w
      " --}}
      src="{{ the_post_thumbnail_url('medium') }}"
      >
    </div>
  </div>
</article>
