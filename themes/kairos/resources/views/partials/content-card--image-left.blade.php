<article @php post_class() @endphp>
  <div class="card card--image-left {{ ($include_excerpt ? '' : 'card--image-left--small') }}">
    <div class="image-container card--image-left__image {{ ($include_excerpt ? '' : 'card--image-left--small__image') }}">
      {{ the_post_thumbnail('medium') }}
    </div>
    <div class="card--image-left__text {{ ($include_excerpt ? '' :  'card--image-left--small__text') }}">
      <header>
        <a class="card--image-left__title {{ ($include_excerpt ? '' :  'card--image-left--small__title') }}" href="{{ get_permalink() }}"><h2>{!! get_the_title() !!}</h2></a>
        @include('partials/entry-meta')
      </header>
      @if ($include_excerpt)
        <div class="card--image-left__excerpt">
          {!! the_excerpt() !!}
        </div>
      @endif
    </div>
  </div>
</article>
