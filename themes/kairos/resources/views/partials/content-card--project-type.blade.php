<article @php post_class() @endphp>
  <div class="card card--category">
    <div class="image-container">
      {{ the_post_thumbnail('medium') }}
    </div>
    <div>
      <header>
        @include('partials/entry-meta-project-name')
        <h3 class="entry-title"><a href="{{ get_permalink() }}">{!! get_the_title() !!}</a></h3>
      </header>
    </div>
  </div>
</article>
