@php
  $post_type = get_queried_object();
  $archive_slug = $post_type->rewrite['slug'];
@endphp

<div class="page-body">
  {!! the_archive_description() !!}

  @if (have_posts())

  <h2>{!! get_the_archive_title() !!} Resources</h2>

  <div class="archive-squares">
    @php $index = 0 @endphp
    @while (have_posts()) @php the_post() @endphp

      @if (($index % 2) === 0)
        @if ($index > 1)
          </div>
        @endif
        <div class="row">
      @endif

      <div class="col-sm col-md-6">
        @include('partials.content-card--project-name')
      </div>

      @php $index++; @endphp

    @endwhile
  </div>

  <a href="{!! get_post_type_archive_link($archive_slug) !!}" class="btn btn-primary">View All</a>
  @endif
</div>
