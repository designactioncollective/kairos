@php the_content() @endphp

@php
    $args = array(
      'menu_class' => 'menu--child-pages',
      'show_parent' => false,
);
@endphp

<div class="submenu-cards">
  {!! display_the_submenu($args) !!}
</div>
