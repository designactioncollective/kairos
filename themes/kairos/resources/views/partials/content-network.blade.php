<article @php post_class() @endphp>
  <header>
    <h3 class="entry-title"><a href="{{ get_field('url') }}">{!! get_the_title() !!}</a></h3>
  </header>
  {!! do_shortcode('[leaflet-marker address="'.get_field('address').'"]<a class="leaflet-link-title" href="'.get_field('url').'">'.get_the_title().'</a><div>'.get_field('address').'</div>[/leaflet-marker]') !!}
</article>
