<span class="image-container">
  {!! get_avatar( get_the_author_meta('user_email'), $size = '50') !!}
</span>
<a class="post__author__link" href="updates/?authors={!! get_the_author_meta('user_login') !!}">
  <span class="author-name">{!! get_the_author_meta('display_name') !!}</span>
</a>
