<div class="page-body">
  {!! the_archive_description() !!}

  @if (have_posts())
  <div class="archive-squares">
    @php $index = ($has_featured_image) ? -1 : 0; @endphp
    @while (have_posts()) @php the_post() @endphp
      @if ($index == -1)
      <div class="row">
        <div class="col-sm">
          @include('partials.content-card--image-left', array('include_excerpt'=> true))
          {{-- <hr /> --}}
        </div>
      </div>
      @else

      @if (($index % 2) === 0)
        @if ($index > 1)
          </div>
        @endif
        <div class="row">
      @endif

      <div class="col-xs-12 col-md-12 col-lg-6"Holy fuck>
        @include('partials.content-card--image-left', array('include_excerpt'=> false))
      </div>

      @endif

      @php $index++; @endphp

    @endwhile
  </div>

  <div class="row">
    <div class="col-12">
      {!! bootstrap_pagination() !!}
    </div>
  </div>
  @endif
</div>
