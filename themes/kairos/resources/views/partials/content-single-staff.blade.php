<article @php post_class() @endphp>
  <div class="entry-content staff">
    <span class="image-container staff__image">
      {!! the_post_thumbnail('medium') !!}
    </span>
    <header>
      <h2 class="staff__title">
        {{ the_title() }}
      </h2>
      <span class="staff__job">
        {!! get_field('job_title') !!}
      </span>
    </header>
    <div class="staff__content">
      {!! the_content() !!}
    </div>
  </div>
  @php
      $user_id = get_field('author_acf')['ID'];
  @endphp
  <aside>
    @php
      $args = array(
        'author'        =>  $user_id,
        'orderby'       =>  'post_date',
        'order'         =>  'DESC'
      );
      $updates = new WP_Query( $args );
      @endphp
    @if ( $updates->have_posts() )
      <h3 class="staff__updates">Updates by {!! the_title() !!}:</h3>
      <div class="flex-container">
        <div class="row">
          @while ( $updates->have_posts() ) @php $updates->the_post() @endphp
            <div class="col-12 col-lg-6">
              @include('partials.content-card--image-left')
            </div>
          @endwhile
        </div>
      </div>
    @endif
    @php wp_reset_postdata(); @endphp
    @php
      $args = array(
        'author'        =>  $user_id,
        'orderby'       =>  'post_date',
        'order'         =>  'ASC',
        'post_type'     =>  'op-eds'
      );
      $updates = new WP_Query( $args );
      @endphp
    @if ( $updates->have_posts() )
      <h3 class="staff__updates">Op-Eds by {!! the_title() !!}:</h3>
      <div class="flex-container">
        <div class="row">
          @while ( $updates->have_posts() ) @php $updates->the_post() @endphp
            <div class="col-12 col-lg-6">
              @include('partials.content-card--image-left')
            </div>
          @endwhile
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          {!! bootstrap_pagination() !!}
        </div>
      </div>
    @endif
    @php wp_reset_postdata(); @endphp
  </aside>
</article>
