@php
  if (is_post_type_archive('resources_cpt') || is_tax(['resource-project-name', 'resource-type'])) {
    $slug = '658-2';
    $label = 'Search Resources';
  } elseif (is_home() || is_tax('category') || is_author()) {
    $slug = 'updates';
    $label = 'Search Updates';
  } else {
    $slug = '';
    $label = '';
  }
@endphp

<div class="filter__wrap alignfull container-min-width-100">
  <div class="container">
    {{-- <strong class="filter__label">{!! $label !!}</strong> --}}
    {!! do_shortcode('[searchandfilter slug="'.$slug.'"]') !!}
  </div>
</div>
