<article @php post_class() @endphp>
  <div class="card card--square staff">
    <div class="image-container">
      {{ the_post_thumbnail('staff-portrait') }}
    </div>
    {{-- <header> --}}
      <a class="staff__link" href="{{ get_permalink() }}"><h2 class="staff__title entry-title">{!! get_the_title() !!}</h2></a>
      <div class="staff__job">
        {!! get_field('job_title') !!}
      </div>
    {{-- </header> --}}
  </div>
</article>
