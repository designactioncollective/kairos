@php
  $cats = wp_get_post_categories( get_the_ID(), array('fields' => 'all_with_object_id') );
  $firstName = get_the_author_meta('user_login');
  $displayName = get_the_author_meta('display_name');
@endphp

{{-- This will only show one project-name, even if post has many --}}
@if ($cats !== false)
  <div class="project-details {!! $container_class !!}">
    <div>
      <span class="project-details__name"><a href="/resources_cpt/?_sft_resource-project-name={!! $cats[0]->slug !!}">{!! $cats[0]->name !!}</a> / </span><span class="project-details__type"><a class="post__author__link" href="updates/?authors={!! $firstName !!}">{!! $displayName !!}</a></span>
    </div>
  </div>
@endif
