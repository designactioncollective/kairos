@extends('layouts.front-page')

@section('content')
  @include('partials.front-page/slider')
  @include('partials.front-page/updates')
  @include('partials.front-page/projects')
  @include('partials.front-page/take-action')
  @include('partials.front-page/resources')
  @include('partials.front-page/social-media-feeds')
  @include('partials.front-page/blurb')
@endsection
