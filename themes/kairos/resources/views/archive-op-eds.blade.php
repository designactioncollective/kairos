@extends('layouts.app')

@section('content')
  @include('partials.pressroom-cpt-archives', array('has_featured_image'=> true))
@endsection
