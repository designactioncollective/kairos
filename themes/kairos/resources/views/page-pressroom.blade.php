@extends('layouts.app')

{{-- TODO: Can put this in a controller? --}}
@php
  $has_featured_image = true;
@endphp

@section('content')
  @while(have_posts()) @php the_post() @endphp
    <div class="page-body">

      @include('partials.pressroom-section', array('title' => 'Press Releases', 'slug' => 'press-releases'))
      @include('partials.pressroom-section', array('title' => 'Media Coverage', 'slug' => 'media-coverage'))
      @include('partials.pressroom-section', array('title' => 'Publications', 'slug' => 'publication'))
      @include('partials.pressroom-section', array('title' => 'Op-Eds', 'slug' => 'op-eds'))

    </div>
  @endwhile
@endsection
