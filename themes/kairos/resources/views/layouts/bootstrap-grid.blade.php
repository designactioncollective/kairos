@yield('var_ratio')
@yield('var_content_template')

<div class="container">
  @yield('title')

  @php $index = 0; @endphp

  @while (have_posts()) @php the_post() @endphp

    @if (get_field('team_or_board') == 1)

      {{-- We want each row to have 3 items. So do this math to work out when to print a new row tag and a closing div --}}
      @if (($index % $ratio) === 0)
        @if ($index > 1)
          </div>
        @endif
        <div class="row">
      @endif

      {{--  TEMPLATE: Use the passed-in number to determine which classes to put here (col-md-4, col-md-3, etc) --}}
      {{-- Print content --}}
      @switch($ratio)
        @case(1)
          <div class="col-sm">
          @break

        @case(2)
          <div class="col-sm col-md-6">
          @break

        @case(3)
          <div class="col-sm col-md-4">
          @break

        @case(4)
          <div class="col-sm col-md-3">
          @break

        @default
          <div class="col-sm">
          @break
      @endswitch
        @include($content_template)
      </div>

      {{-- Increment index --}}
      @php $index++; @endphp

    @endif

  @endwhile
  </div> {{-- Print last closing div for .row --}}
</div> {{-- Print last closing div for .container --}}
