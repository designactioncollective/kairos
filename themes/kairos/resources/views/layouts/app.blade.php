<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  <body @php body_class() @endphp>
    @php do_action('get_header') @endphp
    @include('partials.header')
    <div class="wrap" role="document">
      <div class="content">
        {{-- TODO: Might need to take the page head out of the content, so that the sidebar can sit on top of it on small screens --}}
          {{-- @yield('page-title') --}}
        <div class="page-head container-fluid no-row">
          @include('partials.page-header')
          @yield('filter')
        </div>
        <div class="container container-md">
        {{-- <div class="container"> --}}
          <div class="row">
            {{-- <main class="main col-xs-12 {{ (App\display_sidebar()) ? 'col-md-9' : '' }} container-md__keep-max-width container"> --}}
            <main class="main col-12 {{ (App\display_sidebar()) ? 'col-md-9' : '' }}">
            {{-- <main class="main col-12 {{ (App\display_sidebar()) ? 'col-md-9' : '' }} container"> --}}
              @yield('content')
            </main>
            @if (App\display_sidebar())
              <aside class="sidebar col-xs-12 col-md-3">
                @include('partials.sidebar')
              </aside>
            @endif
            <div class="main-footer container-md__keep-max-width">
            {{-- <div class="main-footer container"> --}}
              @yield('main-footer')
            </div>
          </div>
        </div>
      </div>
    </div>
    @php do_action('get_footer') @endphp
    @include('partials.footer')
    @php wp_footer() @endphp
  </body>
</html>
