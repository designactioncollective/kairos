<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  <body @php body_class() @endphp>
    <div class="kairos-swoosh-background-image">
    {{-- <div> --}}
      {{-- <img class="" src="../images/kairos_background-min.svg" alt=""> --}}
      @php do_action('get_header') @endphp
      @include('partials.header')
      <div class="wrap" role="document">
        <div class="content">
          <main class="main">
            @yield('content')
          </main>
        </div>
      </div>
      @php do_action('get_footer') @endphp
      @include('partials.footer')
      @php wp_footer() @endphp
    </div>
  </body>
</html>
