@extends('layouts.app')

@section('filter')
  @if (is_post_type_archive('resources_cpt') || is_tax(['resource-project-name', 'resource-type']))
    @include('partials.filter')
  @endif
@endsection

@section('content')
  <div class="page-body results">
    {!! the_archive_description() !!}

    @php $index = 0; @endphp
    @while (have_posts()) @php the_post() @endphp

      @if (($index % 2) === 0)
        @if ($index > 1)
          </div>
        @endif
        <div class="row">
      @endif

      <div class="col-12 col-sm-6">
        @include('partials.content-'.get_post_type())
      </div>

      @php $index++; @endphp

    @endwhile

    @if (($index % 2) === 0)
      @if ($index > 1)
        </div>
      @endif
    @endif

    <div class="row">
      <div class="col-12">
        {!! bootstrap_pagination() !!}
      </div>
    </div>
  </div>
@endsection
