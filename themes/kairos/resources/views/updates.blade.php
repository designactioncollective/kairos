@extends('layouts.app')

{{-- <div class="page-head container-fluid no-row">
  @include('partials.page-header')
</div> --}}

@section('content')
  @while(have_posts()) @php the_post() @endphp
    <div class="page-body container no-row">
      @include('partials.content-page')
    </div>

  @endwhile

  {{-- {!! paginate_links() !!} --}}
  <div class="row">
    <div class="col-12">
      {!! bootstrap_pagination() !!}
    </div>
  </div>

@endsection
