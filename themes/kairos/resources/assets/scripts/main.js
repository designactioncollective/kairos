// import external dependencies
import "jquery";

// Import everything from autoload
import "./autoload/**/*";

// import local dependencies
import Router from "./util/Router";
import common from "./routes/common";
import postTypeArchiveStaff from "./routes/postTypeArchiveStaff";

// import then needed Font Awesome functionality
import {
  library,
  dom,
} from "@fortawesome/fontawesome-svg-core";

import {
  faSearch,
  faPlay,
  faPause,
} from "@fortawesome/pro-regular-svg-icons";

// import {
//   faPlay,
// } from "@fortawesome/pro-solid-svg-icons";

import {
  faChevronDown,
} from "@fortawesome/pro-light-svg-icons";

import {
  faFacebook,
  faInstagram,
  faTwitter,
} from "@fortawesome/free-brands-svg-icons";

// add the imported icons to the library
// library.add(fal, far);
library.add(faFacebook, faInstagram, faTwitter, faSearch,
  faChevronDown, faPause, faPlay);

// tell FontAwesome to watch the DOM and add the SVGs when it detects icon markup
dom.watch();

/** Populate Router instance with DOM routes */
const routes = new Router({
  // All pages
  common,
  postTypeArchiveStaff,
});

// Load Events
jQuery(document).ready(() => routes.loadEvents());
