export default {
  init() {
    // Get all instances of that button
    let buttons = jQuery("[data-rest-slug]");
    buttons.each(function(key, button) {
      let jButton = jQuery(button);
      jButton.click(function() {
        jQuery.ajax({
          url: `${
            location.origin
          }/wp-json/wp/v2/staff?slug=${jButton.attr(
            "data-rest-slug"
          )}&_fields=title,content,acf`,
          dataType: "json",
          success: function(raw) {
            let data = raw[0];
            jQuery("#mName").html(data.title.rendered);
            jQuery("#mJobTitle").html(data.acf.job_title);
            jQuery("#mFeaturedImage").attr(
              "src",
              jButton.attr("data-modal-image")
            );
            jQuery("#mBody").html(data.content.rendered);
            jQuery("#mPhone").html(data.acf.phone_number);
            jQuery("#mEmail").html(data.acf.email);
          },
          // statusCode: {
          //   404: function() {
          //     console.log('404 back');
          //   },
          // },8
        });
      });
    });

    // When data is fetched, update content in modal with it
  },
};
