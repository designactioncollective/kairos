export default {
  init() {
    // JavaScript to be fired on all pages
    let body = document.querySelector("body");
    let hamburger_icon = document.getElementById("hamburger-icon");
    let menu_toggle = document.getElementById("menu-toggle");

    //
    // Toggle flyout on click of menu button or hamburger icon
    //
    hamburger_icon.addEventListener("click", toggleFlyout);
    menu_toggle.addEventListener("click", toggleFlyout);

    //
    // Show the menu button if it is focused
    //
    menu_toggle.addEventListener(
      "focus",
      function() {
        menu_toggle.classList.remove("visually-hidden");
        menu_toggle.classList.add("visually-visible");
      }
    );

    //
    // Hide the menu button if it is unfocused
    //
    menu_toggle.addEventListener(
      "blur",
      function() {
        menu_toggle.classList.add("visually-hidden");
        menu_toggle.classList.remove("visually-visible");
      }
    );

    //
    // Toggle flyout if enter pressed on menu button
    //
    menu_toggle.addEventListener(
      "keyup",
      function(e) {
        if ( e.keyCode == 13) {
          e.preventDefault();
          showFlyout();
          return false;
        }
      }
    );

    //
    // Hide flyout if escape pressed while it is open
    //
    body.addEventListener(
      "keyup",
      function(e) {
        if ( e.keyCode == 27 && body.classList.contains("flyout-open") ) {
          e.preventDefault();
          hideFlyout();
          return false;
        }
      }
    );
  },
  finalize() {
    setupOwlSlider();
  },
};

function setupOwlSlider() {
  // https://owlcarousel2.github.io/OwlCarousel2/docs/started-installation.html
  jQuery(document).ready(function(){
    var owl = jQuery(".owl-carousel");

    owl.owlCarousel({
      loop:true,
      responsiveClass:true,
      nav:true,
      dots:false,
      autoplay: true,
      autoplayHoverPause: true,
      smartSpeed: 300,
      navSpeed: 300,
      autoplaySpeed:3200,
      navText: [
        `
          <svg width="16px" height="26px" viewBox="0 0 16 26" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
          <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
              <g id="Kairos-Slider-Home-Desktop-w/hovers" transform="translate(-111.000000, -788.000000)" fill="#790314" fill-rule="nonzero">
                  <g id="Slider" transform="translate(0.000000, 267.000000)">
                      <g id="Slider-Buttons" transform="translate(95.000000, 510.000000)">
                          <g id="angle-left-light" transform="translate(16.087399, 11.000000)">
                              <path d="M0.356223722,12.0357391 L12.2182615,0.354954866 C12.6915347,-0.118318289 13.4568274,-0.118318289 13.9301006,0.354954866 L14.6450451,1.06989942 C15.1183183,1.54317257 15.1183183,2.30846533 14.6450451,2.78173849 L4.34380179,12.8916586 L14.6349755,23.0015788 C15.1082486,23.4748519 15.1082486,24.2401447 14.6349755,24.7134179 L13.9200309,25.4283624 C13.4467578,25.9016356 12.681465,25.9016356 12.2081919,25.4283624 L0.346154081,13.7475782 C-0.117049432,13.274305 -0.117049432,12.5090123 0.356223722,12.0357391 Z" id="Path"></path>
                          </g>
                      </g>
                  </g>
              </g>
          </g>
          </svg>
        `,
        `
        <svg width="15px" height="26px" viewBox="0 0 15 26" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
            <g id="Kairos-Slider-Home-Desktop" transform="translate(-165.000000, -788.000000)" fill="#790314" fill-rule="nonzero">
                <g id="Slider" transform="translate(0.000000, 267.000000)">
                    <g id="Slider-Buttons" transform="translate(95.000000, 510.000000)">
                        <g id="Right-Button" transform="translate(53.000000, 0.000000)">
                            <g id="angle-right-light" transform="translate(17.000000, 11.000000)">
                                <path d="M14.6437763,13.5617407 L2.78173849,25.084626 C2.30846533,25.5515015 1.54317257,25.5515015 1.06989942,25.084626 L0.354954866,24.3793459 C-0.118318289,23.9124704 -0.118318289,23.1575227 0.354954866,22.6906472 L10.6561982,12.7173913 L0.365024508,2.7441354 C-0.108248647,2.27725988 -0.108248647,1.52231222 0.365024508,1.05543669 L1.07996906,0.350156644 C1.55324221,-0.116718881 2.31853498,-0.116718881 2.79180813,0.350156644 L14.6538459,11.873042 C15.1170494,12.3399175 15.1170494,13.0948651 14.6437763,13.5617407 Z" id="Path"></path>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
        </svg>
        `,
      ],
      //responsiveBaseElement:'.wrap',
      responsive:{
        0:{
            items:1,
            nav:true,
        },
      },
    });

    jQuery(".owl-nav").addClass("container");
    createSliderPausePlayButtons();
    jQuery(".owlslider__pause-btn").on("click", pauseSlideShow);
    jQuery(".owlslider__play-btn").on("click", playSlideShow);
  });
}

function createSliderPausePlayButtons() {
  // MV note: Putting templates in here is stupid but that's fine for now. Ideally I would put this template in an HTML file somewhere so no one's confused about where to find it if they need to edit it.
  jQuery(".owl-nav").append(`
    <button class="slider_pause-play-btn owlslider__pause-btn" aria-label="Stop the automatic slideshow">
      <i class="far fa-pause"></i>
    </button>
  `).append(`
    <button class="slider_pause-play-btn owlslider__play-btn" aria-label="Start the automatic slideshow" style="display:none;">
      <i class="far fa-play"></i>
    </button>
  `);
}

function pauseSlideShow() {
  let owl = jQuery(".owl-carousel");
  owl.trigger("stop.owl.autoplay");
  jQuery(".owlslider__pause-btn").hide();
  jQuery(".owlslider__play-btn").show();
}

function playSlideShow() {
  let owl = jQuery(".owl-carousel");
  owl.trigger("play.owl.autoplay");
  jQuery(".owlslider__pause-btn").show();
  jQuery(".owlslider__play-btn").hide();
}

function showFlyout() {

  let menu_toggle = document.getElementById("menu-toggle");
  let flyout = document.getElementById("flyout-menu");
  let body = document.querySelector("body");
  // let flyout_links = document.querySelectorAll('.a11y-menu a');

  body.classList.add("flyout-open");
  menu_toggle.setAttribute("aria-expanded", true);
  flyout.setAttribute("aria-hidden", false);

  // for (let index = 0; index < flyout_links.length; index++) {
  //   flyout_links[index].tabIndex = 0;
  // }
}

function hideFlyout() {
  let open_dropdowns = document.querySelectorAll("#flyout-menu [aria-expanded=\"true\"]");
  // let flyout_links = document.querySelectorAll('.a11y-menu a');

  // for (let index = 0; index < flyout_links.length; index++) {
  //   flyout_links[index].tabIndex = -1;
  // }

  for (let index = 0; index < open_dropdowns.length; index++) {
    open_dropdowns[index].click();
  }

  let menu_toggle = document.getElementById("menu-toggle");
  let flyout = document.getElementById("flyout-menu");
  let body = document.querySelector("body");

  body.classList.remove("flyout-open");
  menu_toggle.setAttribute("aria-expanded", false);
  flyout.setAttribute("aria-hidden", true);

}

function toggleFlyout() {

  let body = document.querySelector("body");


  if ( body.classList.contains("flyout-open") ) {
    hideFlyout();
  }
  else {
    showFlyout();
  }
}

// TODO: Figure out how to put this in the util directory and just import into here. Have to convert funcs into classes? Have to `export`?
/* eslint-disable */
/*
*   This content is licensed according to the W3C Software License at
*   https://www.w3.org/Consortium/Legal/2015/copyright-software-and-document
*
*   Supplemental JS for the disclosure menu keyboard behavior
*/

var DisclosureNav = function (domNode) {
  this.rootNode = domNode;
  this.triggerNodes = [];
  this.controlledNodes = [];
  this.openIndex = null;
  this.useArrowKeys = true;
};

DisclosureNav.prototype.init = function () {
  var buttons = this.rootNode.querySelectorAll('button[aria-expanded][aria-controls]');
  for (var i = 0; i < buttons.length; i++) {
    var button = buttons[i];
    var menu = button.parentNode.parentNode.querySelector('ul'); // Edited to allow grouping link name w/ arrow
    if (menu) {
      // save ref to button and controlled menu
      this.triggerNodes.push(button);
      this.controlledNodes.push(menu);

      // collapse menus
      button.setAttribute('aria-expanded', 'false');
      this.toggleMenu(menu, false);

      // attach event listeners
      menu.addEventListener('keydown', this.handleMenuKeyDown.bind(this));
      button.addEventListener('click', this.handleButtonClick.bind(this));
      button.addEventListener('keydown', this.handleButtonKeyDown.bind(this));
    }
  }

  this.rootNode.addEventListener('focusout', this.handleBlur.bind(this));
};

DisclosureNav.prototype.toggleMenu = function (domNode, show) {
  if (domNode) {
    domNode.dataset['open'] = show ? true : false;
    domNode.style.display = show ? 'block' : 'none';
    domNode.style.visibility = show ? 'visible' : 'hidden'; // Line added to allow transition on mobile screen
  }
};

DisclosureNav.prototype.toggleExpand = function (index, expanded) {
  // close open menu, if applicable
  if (this.openIndex !== index) {
    this.toggleExpand(this.openIndex, false);
  }

  // handle menu at called index
  if (this.triggerNodes[index]) {
    this.openIndex = expanded ? index : null;
    this.triggerNodes[index].setAttribute('aria-expanded', expanded);
    this.toggleMenu(this.controlledNodes[index], expanded);
  }
};

DisclosureNav.prototype.controlFocusByKey = function (keyboardEvent, nodeList, currentIndex) {
  switch (keyboardEvent.key) {
    case 'ArrowUp':
    case 'ArrowLeft':
      keyboardEvent.preventDefault();
      if (currentIndex > -1) {
        var prevIndex = Math.max(0, currentIndex - 1);
        nodeList[prevIndex].focus();
      }
      break;
    case 'ArrowDown':
    case 'ArrowRight':
      keyboardEvent.preventDefault();
      if (currentIndex > -1) {
        var nextIndex = Math.min(nodeList.length - 1, currentIndex + 1);
        nodeList[nextIndex].focus();
      }
      break;
    case 'Home':
      keyboardEvent.preventDefault();
      nodeList[0].focus();
      break;
    case 'End':
      keyboardEvent.preventDefault();
      nodeList[nodeList.length - 1].focus();
      break;
  }
};

/* Event Handlers */
DisclosureNav.prototype.handleBlur = function (event) {
  var menuContainsFocus = this.rootNode.contains(event.relatedTarget);
  if (!menuContainsFocus && this.openIndex !== null) {
    this.toggleExpand(this.openIndex, false);
  }
};

DisclosureNav.prototype.handleButtonKeyDown = function (event) {
  var targetButtonIndex = this.triggerNodes.indexOf(document.activeElement);

  // close on escape
  if (event.key === 'Escape') {
    this.toggleExpand(this.openIndex, false);
  }

  // move focus into the open menu if the current menu is open
  else if (this.useArrowKeys && this.openIndex === targetButtonIndex && event.key === 'ArrowDown') {
    event.preventDefault();
    this.controlledNodes[this.openIndex].querySelector('a').focus();
  }

  // handle arrow key navigation between top-level buttons, if set
  else if (this.useArrowKeys) {
    this.controlFocusByKey(event, this.triggerNodes, targetButtonIndex);
  }
};

DisclosureNav.prototype.handleButtonClick = function (event) {
  var button = event.target;
  var buttonIndex = this.triggerNodes.indexOf(button);
  var buttonExpanded = button.getAttribute('aria-expanded') === 'true';
  this.toggleExpand(buttonIndex, !buttonExpanded);
};

DisclosureNav.prototype.handleMenuKeyDown = function (event) {
  if (this.openIndex === null) {
    return;
  }

  var menuLinks = Array.prototype.slice.call(this.controlledNodes[this.openIndex].querySelectorAll('a'));
  var currentIndex = menuLinks.indexOf(document.activeElement);

  // close on escape
  if (event.key === 'Escape') {
    this.triggerNodes[this.openIndex].focus();
    this.toggleExpand(this.openIndex, false);
  }

  // handle arrow key navigation within menu links, if set
  else if (this.useArrowKeys) {
    this.controlFocusByKey(event, menuLinks, currentIndex);
  }
};

// switch on/off arrow key navigation
DisclosureNav.prototype.updateKeyControls = function (useArrowKeys) {
  this.useArrowKeys = useArrowKeys;
};

/* Initialize Disclosure Menus */

window.addEventListener('load', function (event) {
  var menus = document.querySelectorAll('.disclosure-nav');
  var disclosureMenus = [];

  for (var i = 0; i < menus.length; i++) {
    disclosureMenus[i] = new DisclosureNav(menus[i]);
    disclosureMenus[i].init();
  }

  // listen to arrow key checkbox
  // var arrowKeySwitch = document.getElementById('arrow-behavior-switch');
  // arrowKeySwitch.addEventListener('change', function (event) {
  //   var checked = arrowKeySwitch.checked;
  //   for (var i = 0; i < disclosureMenus.length; i++) {
  //     disclosureMenus[i].updateKeyControls(checked);
  //   }
  // });

  // fake link behavior
  var links = document.querySelectorAll('[href="#mythical-page-content"]');
  var examplePageHeading = document.getElementById('mythical-page-heading');
  for (var k = 0; k < links.length; k++) {
    links[k].addEventListener('click', function (event) {
      var pageTitle = event.target.innerText;
      examplePageHeading.innerText = pageTitle;

      // handle aria-current
      for (var n = 0; n < links.length; n++) {
        links[n].removeAttribute('aria-current');
      }
      this.setAttribute('aria-current', 'page');
    });
  }
}, false);
