<?php

namespace App;

/**
 * Add <body> classes
 */
add_filter('body_class', function (array $classes) {
    /** Add page slug if it doesn't exist */
    if (is_single() || is_page() && !is_front_page()) {
        if (!in_array(basename(get_permalink()), $classes)) {
            $classes[] = basename(get_permalink());
        }
    }

    /** Add class if sidebar is active */
    if (display_sidebar()) {
        $classes[] = 'sidebar-primary';
    }

    /** Clean up class names for custom templates */
    $classes = array_map(function ($class) {
        return preg_replace(['/-blade(-php)?$/', '/^page-template-views/'], '', $class);
    }, $classes);

    return array_filter($classes);
});

/**
 * Add "… Continued" to the excerpt
 */
add_filter('excerpt_more', function () {
    return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
});

/**
 * Template Hierarchy should search for .blade.php files
 */
collect([
    'index', '404', 'archive', 'author', 'category', 'tag', 'taxonomy', 'date', 'home',
    'frontpage', 'page', 'paged', 'search', 'single', 'singular', 'attachment', 'embed'
])->map(function ($type) {
    add_filter("{$type}_template_hierarchy", __NAMESPACE__.'\\filter_templates');
});

/**
 * Render page using Blade
 */
add_filter('template_include', function ($template) {
    collect(['get_header', 'wp_head'])->each(function ($tag) {
        ob_start();
        do_action($tag);
        $output = ob_get_clean();
        remove_all_actions($tag);
        add_action($tag, function () use ($output) {
            echo $output;
        });
    });
    $data = collect(get_body_class())->reduce(function ($data, $class) use ($template) {
        return apply_filters("sage/template/{$class}/data", $data, $template);
    }, []);
    if ($template) {
        echo template($template, $data);
        return get_stylesheet_directory().'/index.php';
    }
    return $template;
}, PHP_INT_MAX);

/**
 * Render comments.blade.php
 */
add_filter('comments_template', function ($comments_template) {
    $comments_template = str_replace(
        [get_stylesheet_directory(), get_template_directory()],
        '',
        $comments_template
    );

    $data = collect(get_body_class())->reduce(function ($data, $class) use ($comments_template) {
        return apply_filters("sage/template/{$class}/data", $data, $comments_template);
    }, []);

    $theme_template = locate_template(["views/{$comments_template}", $comments_template]);

    if ($theme_template) {
        echo template($theme_template, $data);
        return get_stylesheet_directory().'/index.php';
    }

    return $comments_template;
}, 100);

/**
 * Display sidebar only certain pages
 */
add_filter('sage/display_sidebar', function () {
  // Updates archive/single, Resources archive/single, Press release archive/single, Pressroom, Events archive/single,
  if (is_post_type_archive(['resources_cpt'])) {
    return false;
  }
  // Jobs archive/singles, Staff archive/singles, Network page, About page, Projects preview (and sub pages), Campaigns archive/single
  elseif (is_post_type_archive(['jobs', 'staff', 'network', 'campaigns']) || is_singular(['jobs', 'staff', 'network', 'campaigns']) || is_tax(['resource-project-name', 'resource-type'], '') || is_page(['25', '32', '2'])) {
    return true; // Press release page
    // return true;
  }
});

/**
* Remove 'Category:' prefix from category titles
* Put this into app/filter.php
*/
add_filter('get_the_archive_title', function ($title) {
  if (is_category()) {
    $title = single_cat_title('', false);
  } elseif (is_archive()) {
    $title = post_type_archive_title( '', false );
  } elseif (is_post_type_archive()) {
    $obj = get_post_type_object( get_post_type() );
    $title = $obj->labels->archives;
  }
  if (is_tax()){
    $title = single_term_title( '', false );
  }
  // return "block";
  return $title;
});

/**
 * Remove the 'Continue' link from the excerpt
 */
add_filter('excerpt_more', function ($more) {
  global $post;
  return '';
});

///Add CPT to category archive page
add_action( 'pre_get_posts', function ( $query ) {
    if ( $query->is_main_query()
    && ( $query->is_post_type_archive("network") )
    || $query->is_post_type_archive("staff") ) {
        $query->set( 'posts_per_page', -1);
    }

} );
