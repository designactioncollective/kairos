module.exports = {
  extends: 'stylelint-config-standard',
  rules: {
    'selector-pseudo-class-no-unknown': [
      true,
      {
        ignorePseudoClasses: ["focus-visible"],
      }
    ],
    'no-empty-source': null,
    'declaration-empty-line-before': [
      'never',
      {
        ignore: ['after-comment', 'after-declaration'],
      },
    ],
    'property-no-unknown': [
      true,
      {
        ignoreProperties: [
          'scrollbar-width',
          'scrollbar-color',
        ],
      },
    ],
    'at-rule-empty-line-before': [
      'never',
      {
        ignoreAtRules: [
          'extend',
          'at-root',
          'debug',
          'warn',
          'error',
          'if',
          'else',
          'for',
          'each',
          'while',
          'mixin',
          'include',
          'content',
          'return',
          'function',
          'tailwind',
          'apply',
          'responsive',
          'variants',
          'screen',
          'include',
        ],
      },
    ],
    'string-quotes': 'double',
    'no-descending-specificity': null,
    'color-hex-length': 'short',
    'at-rule-no-unknown': [
      true,
      {
        ignoreAtRules: [
          'extend',
          'at-root',
          'debug',
          'warn',
          'error',
          'if',
          'else',
          'for',
          'each',
          'while',
          'mixin',
          'include',
          'content',
          'return',
          'function',
          'tailwind',
          'apply',
          'responsive',
          'variants',
          'screen',
          'include',
        ],
      },
    ],
  },
};
