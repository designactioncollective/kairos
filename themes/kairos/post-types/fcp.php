<?php

/**
 * Registers the `fcp` post type.
 */
function fcp_init() {
	register_post_type(
		'fcp',
		array(
			'labels'                => array(
				'name'                  => __( 'Freedom Church of the Poors', 'kairos' ),
				'singular_name'         => __( 'Freedom Church of the Poor', 'kairos' ),
				'all_items'             => __( 'All Freedom Church of the Poors', 'kairos' ),
				'archives'              => __( 'Freedom Church of the Poor Archives', 'kairos' ),
				'attributes'            => __( 'Freedom Church of the Poor Attributes', 'kairos' ),
				'insert_into_item'      => __( 'Insert into Freedom Church of the Poor', 'kairos' ),
				'uploaded_to_this_item' => __( 'Uploaded to this Freedom Church of the Poor', 'kairos' ),
				'featured_image'        => _x( 'Featured Image', 'fcp', 'kairos' ),
				'set_featured_image'    => _x( 'Set featured image', 'fcp', 'kairos' ),
				'remove_featured_image' => _x( 'Remove featured image', 'fcp', 'kairos' ),
				'use_featured_image'    => _x( 'Use as featured image', 'fcp', 'kairos' ),
				'filter_items_list'     => __( 'Filter Freedom Church of the Poors list', 'kairos' ),
				'items_list_navigation' => __( 'Freedom Church of the Poors list navigation', 'kairos' ),
				'items_list'            => __( 'Freedom Church of the Poors list', 'kairos' ),
				'new_item'              => __( 'New Freedom Church of the Poor', 'kairos' ),
				'add_new'               => __( 'Add New', 'kairos' ),
				'add_new_item'          => __( 'Add New Freedom Church of the Poor', 'kairos' ),
				'edit_item'             => __( 'Edit Freedom Church of the Poor', 'kairos' ),
				'view_item'             => __( 'View Freedom Church of the Poor', 'kairos' ),
				'view_items'            => __( 'View Freedom Church of the Poors', 'kairos' ),
				'search_items'          => __( 'Search Freedom Church of the Poors', 'kairos' ),
				'not_found'             => __( 'No Freedom Church of the Poors found', 'kairos' ),
				'not_found_in_trash'    => __( 'No Freedom Church of the Poors found in trash', 'kairos' ),
				'parent_item_colon'     => __( 'Parent Freedom Church of the Poor:', 'kairos' ),
				'menu_name'             => __( 'Freedom Church of the Poors', 'kairos' ),
			),
			'public'                => true,
			'hierarchical'          => false,
			'show_ui'               => true,
			'show_in_nav_menus'     => true,
			'supports'              => array( 'title', 'editor' ),
			'has_archive'           => true,
			'rewrite'               => true,
			'query_var'             => true,
			'menu_position'         => null,
			'menu_icon'             => 'dashicons-bell',
			'show_in_rest'          => true,
			'rest_base'             => 'fcp',
			'rest_controller_class' => 'WP_REST_Posts_Controller',
		)
	);

}
add_action( 'init', 'fcp_init' );

/**
 * Sets the post updated messages for the `fcp` post type.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `fcp` post type.
 */
function fcp_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['fcp'] = array(
		0  => '', // Unused. Messages start at index 1.
		/* translators: %s: post permalink */
		1  => sprintf( __( 'Freedom Church of the Poor updated. <a target="_blank" href="%s">View Freedom Church of the Poor</a>', 'kairos' ), esc_url( $permalink ) ),
		2  => __( 'Custom field updated.', 'kairos' ),
		3  => __( 'Custom field deleted.', 'kairos' ),
		4  => __( 'Freedom Church of the Poor updated.', 'kairos' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'Freedom Church of the Poor restored to revision from %s', 'kairos' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false, // phpcs:ignore WordPress.Security.NonceVerification.Recommended
		/* translators: %s: post permalink */
		6  => sprintf( __( 'Freedom Church of the Poor published. <a href="%s">View Freedom Church of the Poor</a>', 'kairos' ), esc_url( $permalink ) ),
		7  => __( 'Freedom Church of the Poor saved.', 'kairos' ),
		/* translators: %s: post permalink */
		8  => sprintf( __( 'Freedom Church of the Poor submitted. <a target="_blank" href="%s">Preview Freedom Church of the Poor</a>', 'kairos' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		/* translators: 1: Publish box date format, see https://secure.php.net/date 2: Post permalink */
		9  => sprintf( __( 'Freedom Church of the Poor scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Freedom Church of the Poor</a>', 'kairos' ), date_i18n( __( 'M j, Y @ G:i', 'kairos' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		/* translators: %s: post permalink */
		10 => sprintf( __( 'Freedom Church of the Poor draft updated. <a target="_blank" href="%s">Preview Freedom Church of the Poor</a>', 'kairos' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'fcp_updated_messages' );

/**
 * Sets the bulk post updated messages for the `fcp` post type.
 *
 * @param  array $bulk_messages Arrays of messages, each keyed by the corresponding post type. Messages are
 *                              keyed with 'updated', 'locked', 'deleted', 'trashed', and 'untrashed'.
 * @param  int[] $bulk_counts   Array of item counts for each message, used to build internationalized strings.
 * @return array Bulk messages for the `fcp` post type.
 */
function fcp_bulk_updated_messages( $bulk_messages, $bulk_counts ) {
	global $post;

	$bulk_messages['fcp'] = array(
		/* translators: %s: Number of Freedom Church of the Poors. */
		'updated'   => _n( '%s Freedom Church of the Poor updated.', '%s Freedom Church of the Poors updated.', $bulk_counts['updated'], 'kairos' ),
		'locked'    => ( 1 === $bulk_counts['locked'] ) ? __( '1 Freedom Church of the Poor not updated, somebody is editing it.', 'kairos' ) :
						/* translators: %s: Number of Freedom Church of the Poors. */
						_n( '%s Freedom Church of the Poor not updated, somebody is editing it.', '%s Freedom Church of the Poors not updated, somebody is editing them.', $bulk_counts['locked'], 'kairos' ),
		/* translators: %s: Number of Freedom Church of the Poors. */
		'deleted'   => _n( '%s Freedom Church of the Poor permanently deleted.', '%s Freedom Church of the Poors permanently deleted.', $bulk_counts['deleted'], 'kairos' ),
		/* translators: %s: Number of Freedom Church of the Poors. */
		'trashed'   => _n( '%s Freedom Church of the Poor moved to the Trash.', '%s Freedom Church of the Poors moved to the Trash.', $bulk_counts['trashed'], 'kairos' ),
		/* translators: %s: Number of Freedom Church of the Poors. */
		'untrashed' => _n( '%s Freedom Church of the Poor restored from the Trash.', '%s Freedom Church of the Poors restored from the Trash.', $bulk_counts['untrashed'], 'kairos' ),
	);

	return $bulk_messages;
}
add_filter( 'bulk_post_updated_messages', 'fcp_bulk_updated_messages', 10, 2 );
