<?php

/**
 * Registers the `campaigns` post type.
 */
function campaigns_init() {
	register_post_type(
		'campaigns',
		array(
			'labels'                => array(
				'name'                  => __( 'Campaigns', 'kairos' ),
				'singular_name'         => __( 'Campaigns', 'kairos' ),
				'all_items'             => __( 'All Campaigns', 'kairos' ),
				'archives'              => __( 'Campaigns Archives', 'kairos' ),
				'attributes'            => __( 'Campaigns Attributes', 'kairos' ),
				'insert_into_item'      => __( 'Insert into Campaigns', 'kairos' ),
				'uploaded_to_this_item' => __( 'Uploaded to this Campaigns', 'kairos' ),
				'featured_image'        => _x( 'Featured Image', 'campaigns', 'kairos' ),
				'set_featured_image'    => _x( 'Set featured image', 'campaigns', 'kairos' ),
				'remove_featured_image' => _x( 'Remove featured image', 'campaigns', 'kairos' ),
				'use_featured_image'    => _x( 'Use as featured image', 'campaigns', 'kairos' ),
				'filter_items_list'     => __( 'Filter Campaigns list', 'kairos' ),
				'items_list_navigation' => __( 'Campaigns list navigation', 'kairos' ),
				'items_list'            => __( 'Campaigns list', 'kairos' ),
				'new_item'              => __( 'New Campaigns', 'kairos' ),
				'add_new'               => __( 'Add New', 'kairos' ),
				'add_new_item'          => __( 'Add New Campaigns', 'kairos' ),
				'edit_item'             => __( 'Edit Campaigns', 'kairos' ),
				'view_item'             => __( 'View Campaigns', 'kairos' ),
				'view_items'            => __( 'View Campaigns', 'kairos' ),
				'search_items'          => __( 'Search Campaigns', 'kairos' ),
				'not_found'             => __( 'No Campaigns found', 'kairos' ),
				'not_found_in_trash'    => __( 'No Campaigns found in trash', 'kairos' ),
				'parent_item_colon'     => __( 'Parent Campaigns:', 'kairos' ),
				'menu_name'             => __( 'Campaigns', 'kairos' ),
			),
			'public'                => true,
			'hierarchical'          => false,
			'show_ui'               => true,
			'show_in_nav_menus'     => true,
			'supports'              => array( 'title', 'editor' ),
			'has_archive'           => true,
			'rewrite'               => true,
			'query_var'             => true,
			'menu_position'         => null,
			'menu_icon'             => 'dashicons-megaphone',
			'show_in_rest'          => true,
			'rest_base'             => 'campaigns',
			'rest_controller_class' => 'WP_REST_Posts_Controller',
		)
	);

}
add_action( 'init', 'campaigns_init' );

/**
 * Sets the post updated messages for the `campaigns` post type.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `campaigns` post type.
 */
function campaigns_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['campaigns'] = array(
		0  => '', // Unused. Messages start at index 1.
		/* translators: %s: post permalink */
		1  => sprintf( __( 'Campaigns updated. <a target="_blank" href="%s">View Campaigns</a>', 'kairos' ), esc_url( $permalink ) ),
		2  => __( 'Custom field updated.', 'kairos' ),
		3  => __( 'Custom field deleted.', 'kairos' ),
		4  => __( 'Campaigns updated.', 'kairos' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'Campaigns restored to revision from %s', 'kairos' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false, // phpcs:ignore WordPress.Security.NonceVerification.Recommended
		/* translators: %s: post permalink */
		6  => sprintf( __( 'Campaigns published. <a href="%s">View Campaigns</a>', 'kairos' ), esc_url( $permalink ) ),
		7  => __( 'Campaigns saved.', 'kairos' ),
		/* translators: %s: post permalink */
		8  => sprintf( __( 'Campaigns submitted. <a target="_blank" href="%s">Preview Campaigns</a>', 'kairos' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		/* translators: 1: Publish box date format, see https://secure.php.net/date 2: Post permalink */
		9  => sprintf( __( 'Campaigns scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Campaigns</a>', 'kairos' ), date_i18n( __( 'M j, Y @ G:i', 'kairos' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		/* translators: %s: post permalink */
		10 => sprintf( __( 'Campaigns draft updated. <a target="_blank" href="%s">Preview Campaigns</a>', 'kairos' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'campaigns_updated_messages' );

/**
 * Sets the bulk post updated messages for the `campaigns` post type.
 *
 * @param  array $bulk_messages Arrays of messages, each keyed by the corresponding post type. Messages are
 *                              keyed with 'updated', 'locked', 'deleted', 'trashed', and 'untrashed'.
 * @param  int[] $bulk_counts   Array of item counts for each message, used to build internationalized strings.
 * @return array Bulk messages for the `campaigns` post type.
 */
function campaigns_bulk_updated_messages( $bulk_messages, $bulk_counts ) {
	global $post;

	$bulk_messages['campaigns'] = array(
		/* translators: %s: Number of Campaigns. */
		'updated'   => _n( '%s Campaigns updated.', '%s Campaigns updated.', $bulk_counts['updated'], 'kairos' ),
		'locked'    => ( 1 === $bulk_counts['locked'] ) ? __( '1 Campaigns not updated, somebody is editing it.', 'kairos' ) :
						/* translators: %s: Number of Campaigns. */
						_n( '%s Campaigns not updated, somebody is editing it.', '%s Campaigns not updated, somebody is editing them.', $bulk_counts['locked'], 'kairos' ),
		/* translators: %s: Number of Campaigns. */
		'deleted'   => _n( '%s Campaigns permanently deleted.', '%s Campaigns permanently deleted.', $bulk_counts['deleted'], 'kairos' ),
		/* translators: %s: Number of Campaigns. */
		'trashed'   => _n( '%s Campaigns moved to the Trash.', '%s Campaigns moved to the Trash.', $bulk_counts['trashed'], 'kairos' ),
		/* translators: %s: Number of Campaigns. */
		'untrashed' => _n( '%s Campaigns restored from the Trash.', '%s Campaigns restored from the Trash.', $bulk_counts['untrashed'], 'kairos' ),
	);

	return $bulk_messages;
}
add_filter( 'bulk_post_updated_messages', 'campaigns_bulk_updated_messages', 10, 2 );
