<?php

/**
 * Registers the `press_releases` post type.
 */
function press_releases_init() {
	register_post_type(
		'press-releases',
		array(
			'labels'                => array(
				'name'                  => __( 'Press Releases', 'kairos' ),
				'singular_name'         => __( 'Press Releases', 'kairos' ),
				'all_items'             => __( 'All Press Releases', 'kairos' ),
				'archives'              => __( 'Press Releases Archives', 'kairos' ),
				'attributes'            => __( 'Press Releases Attributes', 'kairos' ),
				'insert_into_item'      => __( 'Insert into Press Releases', 'kairos' ),
				'uploaded_to_this_item' => __( 'Uploaded to this Press Releases', 'kairos' ),
				'featured_image'        => _x( 'Featured Image', 'press-releases', 'kairos' ),
				'set_featured_image'    => _x( 'Set featured image', 'press-releases', 'kairos' ),
				'remove_featured_image' => _x( 'Remove featured image', 'press-releases', 'kairos' ),
				'use_featured_image'    => _x( 'Use as featured image', 'press-releases', 'kairos' ),
				'filter_items_list'     => __( 'Filter Press Releases list', 'kairos' ),
				'items_list_navigation' => __( 'Press Releases list navigation', 'kairos' ),
				'items_list'            => __( 'Press Releases list', 'kairos' ),
				'new_item'              => __( 'New Press Releases', 'kairos' ),
				'add_new'               => __( 'Add New', 'kairos' ),
				'add_new_item'          => __( 'Add New Press Releases', 'kairos' ),
				'edit_item'             => __( 'Edit Press Releases', 'kairos' ),
				'view_item'             => __( 'View Press Releases', 'kairos' ),
				'view_items'            => __( 'View Press Releases', 'kairos' ),
				'search_items'          => __( 'Search Press Releases', 'kairos' ),
				'not_found'             => __( 'No Press Releases found', 'kairos' ),
				'not_found_in_trash'    => __( 'No Press Releases found in trash', 'kairos' ),
				'parent_item_colon'     => __( 'Parent Press Releases:', 'kairos' ),
				'menu_name'             => __( 'Press Releases', 'kairos' ),
			),
			'public'                => true,
			'hierarchical'          => false,
			'show_ui'               => true,
			'show_in_nav_menus'     => true,
			'supports'              => array( 'title', 'editor' ),
			'has_archive'           => true,
			'rewrite'               => true,
			'query_var'             => true,
			'menu_position'         => null,
			'menu_icon'             => 'dashicons-format-aside',
			'show_in_rest'          => true,
			'rest_base'             => 'press-releases',
			'rest_controller_class' => 'WP_REST_Posts_Controller',
		)
	);

}
add_action( 'init', 'press_releases_init' );

/**
 * Sets the post updated messages for the `press_releases` post type.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `press_releases` post type.
 */
function press_releases_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['press-releases'] = array(
		0  => '', // Unused. Messages start at index 1.
		/* translators: %s: post permalink */
		1  => sprintf( __( 'Press Releases updated. <a target="_blank" href="%s">View Press Releases</a>', 'kairos' ), esc_url( $permalink ) ),
		2  => __( 'Custom field updated.', 'kairos' ),
		3  => __( 'Custom field deleted.', 'kairos' ),
		4  => __( 'Press Releases updated.', 'kairos' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'Press Releases restored to revision from %s', 'kairos' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false, // phpcs:ignore WordPress.Security.NonceVerification.Recommended
		/* translators: %s: post permalink */
		6  => sprintf( __( 'Press Releases published. <a href="%s">View Press Releases</a>', 'kairos' ), esc_url( $permalink ) ),
		7  => __( 'Press Releases saved.', 'kairos' ),
		/* translators: %s: post permalink */
		8  => sprintf( __( 'Press Releases submitted. <a target="_blank" href="%s">Preview Press Releases</a>', 'kairos' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		/* translators: 1: Publish box date format, see https://secure.php.net/date 2: Post permalink */
		9  => sprintf( __( 'Press Releases scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Press Releases</a>', 'kairos' ), date_i18n( __( 'M j, Y @ G:i', 'kairos' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		/* translators: %s: post permalink */
		10 => sprintf( __( 'Press Releases draft updated. <a target="_blank" href="%s">Preview Press Releases</a>', 'kairos' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'press_releases_updated_messages' );

/**
 * Sets the bulk post updated messages for the `press_releases` post type.
 *
 * @param  array $bulk_messages Arrays of messages, each keyed by the corresponding post type. Messages are
 *                              keyed with 'updated', 'locked', 'deleted', 'trashed', and 'untrashed'.
 * @param  int[] $bulk_counts   Array of item counts for each message, used to build internationalized strings.
 * @return array Bulk messages for the `press_releases` post type.
 */
function press_releases_bulk_updated_messages( $bulk_messages, $bulk_counts ) {
	global $post;

	$bulk_messages['press-releases'] = array(
		/* translators: %s: Number of Press Releases. */
		'updated'   => _n( '%s Press Releases updated.', '%s Press Releases updated.', $bulk_counts['updated'], 'kairos' ),
		'locked'    => ( 1 === $bulk_counts['locked'] ) ? __( '1 Press Releases not updated, somebody is editing it.', 'kairos' ) :
						/* translators: %s: Number of Press Releases. */
						_n( '%s Press Releases not updated, somebody is editing it.', '%s Press Releases not updated, somebody is editing them.', $bulk_counts['locked'], 'kairos' ),
		/* translators: %s: Number of Press Releases. */
		'deleted'   => _n( '%s Press Releases permanently deleted.', '%s Press Releases permanently deleted.', $bulk_counts['deleted'], 'kairos' ),
		/* translators: %s: Number of Press Releases. */
		'trashed'   => _n( '%s Press Releases moved to the Trash.', '%s Press Releases moved to the Trash.', $bulk_counts['trashed'], 'kairos' ),
		/* translators: %s: Number of Press Releases. */
		'untrashed' => _n( '%s Press Releases restored from the Trash.', '%s Press Releases restored from the Trash.', $bulk_counts['untrashed'], 'kairos' ),
	);

	return $bulk_messages;
}
add_filter( 'bulk_post_updated_messages', 'press_releases_bulk_updated_messages', 10, 2 );
