<?php

/**
 * Registers the `publication` post type.
 */
function publication_init() {
	register_post_type(
		'publication',
		array(
			'labels'                => array(
				'name'                  => __( 'Publications', 'kairos' ),
				'singular_name'         => __( 'Publication', 'kairos' ),
				'all_items'             => __( 'All Publications', 'kairos' ),
				'archives'              => __( 'Publication Archives', 'kairos' ),
				'attributes'            => __( 'Publication Attributes', 'kairos' ),
				'insert_into_item'      => __( 'Insert into Publication', 'kairos' ),
				'uploaded_to_this_item' => __( 'Uploaded to this Publication', 'kairos' ),
				'featured_image'        => _x( 'Featured Image', 'publication', 'kairos' ),
				'set_featured_image'    => _x( 'Set featured image', 'publication', 'kairos' ),
				'remove_featured_image' => _x( 'Remove featured image', 'publication', 'kairos' ),
				'use_featured_image'    => _x( 'Use as featured image', 'publication', 'kairos' ),
				'filter_items_list'     => __( 'Filter Publications list', 'kairos' ),
				'items_list_navigation' => __( 'Publications list navigation', 'kairos' ),
				'items_list'            => __( 'Publications list', 'kairos' ),
				'new_item'              => __( 'New Publication', 'kairos' ),
				'add_new'               => __( 'Add New', 'kairos' ),
				'add_new_item'          => __( 'Add New Publication', 'kairos' ),
				'edit_item'             => __( 'Edit Publication', 'kairos' ),
				'view_item'             => __( 'View Publication', 'kairos' ),
				'view_items'            => __( 'View Publications', 'kairos' ),
				'search_items'          => __( 'Search Publications', 'kairos' ),
				'not_found'             => __( 'No Publications found', 'kairos' ),
				'not_found_in_trash'    => __( 'No Publications found in trash', 'kairos' ),
				'parent_item_colon'     => __( 'Parent Publication:', 'kairos' ),
				'menu_name'             => __( 'Publications', 'kairos' ),
			),
			'public'                => true,
			'hierarchical'          => false,
			'show_ui'               => true,
			'show_in_nav_menus'     => true,
			'supports'              => array( 'title', 'editor' ),
			'has_archive'           => true,
			'rewrite'               => true,
			'query_var'             => true,
			'menu_position'         => null,
			'menu_icon'             => 'dashicons-edit-page',
			'show_in_rest'          => true,
			'rest_base'             => 'publication',
			'rest_controller_class' => 'WP_REST_Posts_Controller',
		)
	);

}
add_action( 'init', 'publication_init' );

/**
 * Sets the post updated messages for the `publication` post type.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `publication` post type.
 */
function publication_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['publication'] = array(
		0  => '', // Unused. Messages start at index 1.
		/* translators: %s: post permalink */
		1  => sprintf( __( 'Publication updated. <a target="_blank" href="%s">View Publication</a>', 'kairos' ), esc_url( $permalink ) ),
		2  => __( 'Custom field updated.', 'kairos' ),
		3  => __( 'Custom field deleted.', 'kairos' ),
		4  => __( 'Publication updated.', 'kairos' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'Publication restored to revision from %s', 'kairos' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false, // phpcs:ignore WordPress.Security.NonceVerification.Recommended
		/* translators: %s: post permalink */
		6  => sprintf( __( 'Publication published. <a href="%s">View Publication</a>', 'kairos' ), esc_url( $permalink ) ),
		7  => __( 'Publication saved.', 'kairos' ),
		/* translators: %s: post permalink */
		8  => sprintf( __( 'Publication submitted. <a target="_blank" href="%s">Preview Publication</a>', 'kairos' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		/* translators: 1: Publish box date format, see https://secure.php.net/date 2: Post permalink */
		9  => sprintf( __( 'Publication scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Publication</a>', 'kairos' ), date_i18n( __( 'M j, Y @ G:i', 'kairos' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		/* translators: %s: post permalink */
		10 => sprintf( __( 'Publication draft updated. <a target="_blank" href="%s">Preview Publication</a>', 'kairos' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'publication_updated_messages' );

/**
 * Sets the bulk post updated messages for the `publication` post type.
 *
 * @param  array $bulk_messages Arrays of messages, each keyed by the corresponding post type. Messages are
 *                              keyed with 'updated', 'locked', 'deleted', 'trashed', and 'untrashed'.
 * @param  int[] $bulk_counts   Array of item counts for each message, used to build internationalized strings.
 * @return array Bulk messages for the `publication` post type.
 */
function publication_bulk_updated_messages( $bulk_messages, $bulk_counts ) {
	global $post;

	$bulk_messages['publication'] = array(
		/* translators: %s: Number of Publications. */
		'updated'   => _n( '%s Publication updated.', '%s Publications updated.', $bulk_counts['updated'], 'kairos' ),
		'locked'    => ( 1 === $bulk_counts['locked'] ) ? __( '1 Publication not updated, somebody is editing it.', 'kairos' ) :
						/* translators: %s: Number of Publications. */
						_n( '%s Publication not updated, somebody is editing it.', '%s Publications not updated, somebody is editing them.', $bulk_counts['locked'], 'kairos' ),
		/* translators: %s: Number of Publications. */
		'deleted'   => _n( '%s Publication permanently deleted.', '%s Publications permanently deleted.', $bulk_counts['deleted'], 'kairos' ),
		/* translators: %s: Number of Publications. */
		'trashed'   => _n( '%s Publication moved to the Trash.', '%s Publications moved to the Trash.', $bulk_counts['trashed'], 'kairos' ),
		/* translators: %s: Number of Publications. */
		'untrashed' => _n( '%s Publication restored from the Trash.', '%s Publications restored from the Trash.', $bulk_counts['untrashed'], 'kairos' ),
	);

	return $bulk_messages;
}
add_filter( 'bulk_post_updated_messages', 'publication_bulk_updated_messages', 10, 2 );
