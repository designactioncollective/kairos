<?php

/**
 * Registers the `media_coverage` post type.
 */
function media_coverage_init() {
	register_post_type(
		'media-coverage',
		array(
			'labels'                => array(
				'name'                  => __( 'Media Coverages', 'kairos' ),
				'singular_name'         => __( 'Media Coverage', 'kairos' ),
				'all_items'             => __( 'All Media Coverages', 'kairos' ),
				'archives'              => __( 'Media Coverage Archives', 'kairos' ),
				'attributes'            => __( 'Media Coverage Attributes', 'kairos' ),
				'insert_into_item'      => __( 'Insert into Media Coverage', 'kairos' ),
				'uploaded_to_this_item' => __( 'Uploaded to this Media Coverage', 'kairos' ),
				'featured_image'        => _x( 'Featured Image', 'media-coverage', 'kairos' ),
				'set_featured_image'    => _x( 'Set featured image', 'media-coverage', 'kairos' ),
				'remove_featured_image' => _x( 'Remove featured image', 'media-coverage', 'kairos' ),
				'use_featured_image'    => _x( 'Use as featured image', 'media-coverage', 'kairos' ),
				'filter_items_list'     => __( 'Filter Media Coverages list', 'kairos' ),
				'items_list_navigation' => __( 'Media Coverages list navigation', 'kairos' ),
				'items_list'            => __( 'Media Coverages list', 'kairos' ),
				'new_item'              => __( 'New Media Coverage', 'kairos' ),
				'add_new'               => __( 'Add New', 'kairos' ),
				'add_new_item'          => __( 'Add New Media Coverage', 'kairos' ),
				'edit_item'             => __( 'Edit Media Coverage', 'kairos' ),
				'view_item'             => __( 'View Media Coverage', 'kairos' ),
				'view_items'            => __( 'View Media Coverages', 'kairos' ),
				'search_items'          => __( 'Search Media Coverages', 'kairos' ),
				'not_found'             => __( 'No Media Coverages found', 'kairos' ),
				'not_found_in_trash'    => __( 'No Media Coverages found in trash', 'kairos' ),
				'parent_item_colon'     => __( 'Parent Media Coverage:', 'kairos' ),
				'menu_name'             => __( 'Media Coverages', 'kairos' ),
			),
			'public'                => true,
			'hierarchical'          => false,
			'show_ui'               => true,
			'show_in_nav_menus'     => true,
			'supports'              => array( 'title', 'editor' ),
			'has_archive'           => true,
			'rewrite'               => true,
			'query_var'             => true,
			'menu_position'         => null,
			'menu_icon'             => 'dashicons-video-alt',
			'show_in_rest'          => true,
			'rest_base'             => 'media-coverage',
			'rest_controller_class' => 'WP_REST_Posts_Controller',
		)
	);

}
add_action( 'init', 'media_coverage_init' );

/**
 * Sets the post updated messages for the `media_coverage` post type.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `media_coverage` post type.
 */
function media_coverage_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['media-coverage'] = array(
		0  => '', // Unused. Messages start at index 1.
		/* translators: %s: post permalink */
		1  => sprintf( __( 'Media Coverage updated. <a target="_blank" href="%s">View Media Coverage</a>', 'kairos' ), esc_url( $permalink ) ),
		2  => __( 'Custom field updated.', 'kairos' ),
		3  => __( 'Custom field deleted.', 'kairos' ),
		4  => __( 'Media Coverage updated.', 'kairos' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'Media Coverage restored to revision from %s', 'kairos' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false, // phpcs:ignore WordPress.Security.NonceVerification.Recommended
		/* translators: %s: post permalink */
		6  => sprintf( __( 'Media Coverage published. <a href="%s">View Media Coverage</a>', 'kairos' ), esc_url( $permalink ) ),
		7  => __( 'Media Coverage saved.', 'kairos' ),
		/* translators: %s: post permalink */
		8  => sprintf( __( 'Media Coverage submitted. <a target="_blank" href="%s">Preview Media Coverage</a>', 'kairos' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		/* translators: 1: Publish box date format, see https://secure.php.net/date 2: Post permalink */
		9  => sprintf( __( 'Media Coverage scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Media Coverage</a>', 'kairos' ), date_i18n( __( 'M j, Y @ G:i', 'kairos' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		/* translators: %s: post permalink */
		10 => sprintf( __( 'Media Coverage draft updated. <a target="_blank" href="%s">Preview Media Coverage</a>', 'kairos' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'media_coverage_updated_messages' );

/**
 * Sets the bulk post updated messages for the `media_coverage` post type.
 *
 * @param  array $bulk_messages Arrays of messages, each keyed by the corresponding post type. Messages are
 *                              keyed with 'updated', 'locked', 'deleted', 'trashed', and 'untrashed'.
 * @param  int[] $bulk_counts   Array of item counts for each message, used to build internationalized strings.
 * @return array Bulk messages for the `media_coverage` post type.
 */
function media_coverage_bulk_updated_messages( $bulk_messages, $bulk_counts ) {
	global $post;

	$bulk_messages['media-coverage'] = array(
		/* translators: %s: Number of Media Coverages. */
		'updated'   => _n( '%s Media Coverage updated.', '%s Media Coverages updated.', $bulk_counts['updated'], 'kairos' ),
		'locked'    => ( 1 === $bulk_counts['locked'] ) ? __( '1 Media Coverage not updated, somebody is editing it.', 'kairos' ) :
						/* translators: %s: Number of Media Coverages. */
						_n( '%s Media Coverage not updated, somebody is editing it.', '%s Media Coverages not updated, somebody is editing them.', $bulk_counts['locked'], 'kairos' ),
		/* translators: %s: Number of Media Coverages. */
		'deleted'   => _n( '%s Media Coverage permanently deleted.', '%s Media Coverages permanently deleted.', $bulk_counts['deleted'], 'kairos' ),
		/* translators: %s: Number of Media Coverages. */
		'trashed'   => _n( '%s Media Coverage moved to the Trash.', '%s Media Coverages moved to the Trash.', $bulk_counts['trashed'], 'kairos' ),
		/* translators: %s: Number of Media Coverages. */
		'untrashed' => _n( '%s Media Coverage restored from the Trash.', '%s Media Coverages restored from the Trash.', $bulk_counts['untrashed'], 'kairos' ),
	);

	return $bulk_messages;
}
add_filter( 'bulk_post_updated_messages', 'media_coverage_bulk_updated_messages', 10, 2 );
