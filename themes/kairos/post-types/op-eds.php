<?php

/**
 * Registers the `op_eds` post type.
 */
function op_eds_init() {
	register_post_type(
		'op-eds',
		array(
			'labels'                => array(
				'name'                  => __( '“Op-Eds”s', 'kairos' ),
				'singular_name'         => __( '“Op-Eds”', 'kairos' ),
				'all_items'             => __( 'All “Op-Eds”s', 'kairos' ),
				'archives'              => __( '“Op-Eds” Archives', 'kairos' ),
				'attributes'            => __( '“Op-Eds” Attributes', 'kairos' ),
				'insert_into_item'      => __( 'Insert into “Op-Eds”', 'kairos' ),
				'uploaded_to_this_item' => __( 'Uploaded to this “Op-Eds”', 'kairos' ),
				'featured_image'        => _x( 'Featured Image', 'op-eds', 'kairos' ),
				'set_featured_image'    => _x( 'Set featured image', 'op-eds', 'kairos' ),
				'remove_featured_image' => _x( 'Remove featured image', 'op-eds', 'kairos' ),
				'use_featured_image'    => _x( 'Use as featured image', 'op-eds', 'kairos' ),
				'filter_items_list'     => __( 'Filter “Op-Eds”s list', 'kairos' ),
				'items_list_navigation' => __( '“Op-Eds”s list navigation', 'kairos' ),
				'items_list'            => __( '“Op-Eds”s list', 'kairos' ),
				'new_item'              => __( 'New “Op-Eds”', 'kairos' ),
				'add_new'               => __( 'Add New', 'kairos' ),
				'add_new_item'          => __( 'Add New “Op-Eds”', 'kairos' ),
				'edit_item'             => __( 'Edit “Op-Eds”', 'kairos' ),
				'view_item'             => __( 'View “Op-Eds”', 'kairos' ),
				'view_items'            => __( 'View “Op-Eds”s', 'kairos' ),
				'search_items'          => __( 'Search “Op-Eds”s', 'kairos' ),
				'not_found'             => __( 'No “Op-Eds”s found', 'kairos' ),
				'not_found_in_trash'    => __( 'No “Op-Eds”s found in trash', 'kairos' ),
				'parent_item_colon'     => __( 'Parent “Op-Eds”:', 'kairos' ),
				'menu_name'             => __( '“Op-Eds”s', 'kairos' ),
			),
			'public'                => true,
			'hierarchical'          => false,
			'show_ui'               => true,
			'show_in_nav_menus'     => true,
			'supports'              => array( 'title', 'editor' ),
			'has_archive'           => true,
			'rewrite'               => true,
			'query_var'             => true,
			'menu_position'         => null,
			'menu_icon'             => 'dashicons-admin-post',
			'show_in_rest'          => true,
			'rest_base'             => 'op-eds',
			'rest_controller_class' => 'WP_REST_Posts_Controller',
		)
	);

}
add_action( 'init', 'op_eds_init' );

/**
 * Sets the post updated messages for the `op_eds` post type.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `op_eds` post type.
 */
function op_eds_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['op-eds'] = array(
		0  => '', // Unused. Messages start at index 1.
		/* translators: %s: post permalink */
		1  => sprintf( __( '“Op-Eds” updated. <a target="_blank" href="%s">View “Op-Eds”</a>', 'kairos' ), esc_url( $permalink ) ),
		2  => __( 'Custom field updated.', 'kairos' ),
		3  => __( 'Custom field deleted.', 'kairos' ),
		4  => __( '“Op-Eds” updated.', 'kairos' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( '“Op-Eds” restored to revision from %s', 'kairos' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false, // phpcs:ignore WordPress.Security.NonceVerification.Recommended
		/* translators: %s: post permalink */
		6  => sprintf( __( '“Op-Eds” published. <a href="%s">View “Op-Eds”</a>', 'kairos' ), esc_url( $permalink ) ),
		7  => __( '“Op-Eds” saved.', 'kairos' ),
		/* translators: %s: post permalink */
		8  => sprintf( __( '“Op-Eds” submitted. <a target="_blank" href="%s">Preview “Op-Eds”</a>', 'kairos' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		/* translators: 1: Publish box date format, see https://secure.php.net/date 2: Post permalink */
		9  => sprintf( __( '“Op-Eds” scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview “Op-Eds”</a>', 'kairos' ), date_i18n( __( 'M j, Y @ G:i', 'kairos' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		/* translators: %s: post permalink */
		10 => sprintf( __( '“Op-Eds” draft updated. <a target="_blank" href="%s">Preview “Op-Eds”</a>', 'kairos' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'op_eds_updated_messages' );

/**
 * Sets the bulk post updated messages for the `op_eds` post type.
 *
 * @param  array $bulk_messages Arrays of messages, each keyed by the corresponding post type. Messages are
 *                              keyed with 'updated', 'locked', 'deleted', 'trashed', and 'untrashed'.
 * @param  int[] $bulk_counts   Array of item counts for each message, used to build internationalized strings.
 * @return array Bulk messages for the `op_eds` post type.
 */
function op_eds_bulk_updated_messages( $bulk_messages, $bulk_counts ) {
	global $post;

	$bulk_messages['op-eds'] = array(
		/* translators: %s: Number of “Op-Eds”s. */
		'updated'   => _n( '%s “Op-Eds” updated.', '%s “Op-Eds”s updated.', $bulk_counts['updated'], 'kairos' ),
		'locked'    => ( 1 === $bulk_counts['locked'] ) ? __( '1 “Op-Eds” not updated, somebody is editing it.', 'kairos' ) :
						/* translators: %s: Number of “Op-Eds”s. */
						_n( '%s “Op-Eds” not updated, somebody is editing it.', '%s “Op-Eds”s not updated, somebody is editing them.', $bulk_counts['locked'], 'kairos' ),
		/* translators: %s: Number of “Op-Eds”s. */
		'deleted'   => _n( '%s “Op-Eds” permanently deleted.', '%s “Op-Eds”s permanently deleted.', $bulk_counts['deleted'], 'kairos' ),
		/* translators: %s: Number of “Op-Eds”s. */
		'trashed'   => _n( '%s “Op-Eds” moved to the Trash.', '%s “Op-Eds”s moved to the Trash.', $bulk_counts['trashed'], 'kairos' ),
		/* translators: %s: Number of “Op-Eds”s. */
		'untrashed' => _n( '%s “Op-Eds” restored from the Trash.', '%s “Op-Eds”s restored from the Trash.', $bulk_counts['untrashed'], 'kairos' ),
	);

	return $bulk_messages;
}
add_filter( 'bulk_post_updated_messages', 'op_eds_bulk_updated_messages', 10, 2 );
