<?php

/**
 * Registers the `history` post type.
 */
function history_init() {
	register_post_type(
		'history',
		array(
			'labels'                => array(
				'name'                  => __( 'Histories', 'kairos' ),
				'singular_name'         => __( 'History', 'kairos' ),
				'all_items'             => __( 'All Histories', 'kairos' ),
				'archives'              => __( 'History Archives', 'kairos' ),
				'attributes'            => __( 'History Attributes', 'kairos' ),
				'insert_into_item'      => __( 'Insert into History', 'kairos' ),
				'uploaded_to_this_item' => __( 'Uploaded to this History', 'kairos' ),
				'featured_image'        => _x( 'Featured Image', 'history', 'kairos' ),
				'set_featured_image'    => _x( 'Set featured image', 'history', 'kairos' ),
				'remove_featured_image' => _x( 'Remove featured image', 'history', 'kairos' ),
				'use_featured_image'    => _x( 'Use as featured image', 'history', 'kairos' ),
				'filter_items_list'     => __( 'Filter Histories list', 'kairos' ),
				'items_list_navigation' => __( 'Histories list navigation', 'kairos' ),
				'items_list'            => __( 'Histories list', 'kairos' ),
				'new_item'              => __( 'New History', 'kairos' ),
				'add_new'               => __( 'Add New', 'kairos' ),
				'add_new_item'          => __( 'Add New History', 'kairos' ),
				'edit_item'             => __( 'Edit History', 'kairos' ),
				'view_item'             => __( 'View History', 'kairos' ),
				'view_items'            => __( 'View Histories', 'kairos' ),
				'search_items'          => __( 'Search Histories', 'kairos' ),
				'not_found'             => __( 'No Histories found', 'kairos' ),
				'not_found_in_trash'    => __( 'No Histories found in trash', 'kairos' ),
				'parent_item_colon'     => __( 'Parent History:', 'kairos' ),
				'menu_name'             => __( 'Histories', 'kairos' ),
			),
			'public'                => true,
			'hierarchical'          => false,
			'show_ui'               => true,
			'show_in_nav_menus'     => true,
			'supports'              => array( 'title', 'editor' ),
			'has_archive'           => true,
			'rewrite'               => true,
			'query_var'             => true,
			'menu_position'         => null,
			'menu_icon'             => 'dashicons-admin-site-alt',
			'show_in_rest'          => true,
			'rest_base'             => 'history',
			'rest_controller_class' => 'WP_REST_Posts_Controller',
		)
	);

}
add_action( 'init', 'history_init' );

/**
 * Sets the post updated messages for the `history` post type.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `history` post type.
 */
function history_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['history'] = array(
		0  => '', // Unused. Messages start at index 1.
		/* translators: %s: post permalink */
		1  => sprintf( __( 'History updated. <a target="_blank" href="%s">View History</a>', 'kairos' ), esc_url( $permalink ) ),
		2  => __( 'Custom field updated.', 'kairos' ),
		3  => __( 'Custom field deleted.', 'kairos' ),
		4  => __( 'History updated.', 'kairos' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'History restored to revision from %s', 'kairos' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false, // phpcs:ignore WordPress.Security.NonceVerification.Recommended
		/* translators: %s: post permalink */
		6  => sprintf( __( 'History published. <a href="%s">View History</a>', 'kairos' ), esc_url( $permalink ) ),
		7  => __( 'History saved.', 'kairos' ),
		/* translators: %s: post permalink */
		8  => sprintf( __( 'History submitted. <a target="_blank" href="%s">Preview History</a>', 'kairos' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		/* translators: 1: Publish box date format, see https://secure.php.net/date 2: Post permalink */
		9  => sprintf( __( 'History scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview History</a>', 'kairos' ), date_i18n( __( 'M j, Y @ G:i', 'kairos' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		/* translators: %s: post permalink */
		10 => sprintf( __( 'History draft updated. <a target="_blank" href="%s">Preview History</a>', 'kairos' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'history_updated_messages' );

/**
 * Sets the bulk post updated messages for the `history` post type.
 *
 * @param  array $bulk_messages Arrays of messages, each keyed by the corresponding post type. Messages are
 *                              keyed with 'updated', 'locked', 'deleted', 'trashed', and 'untrashed'.
 * @param  int[] $bulk_counts   Array of item counts for each message, used to build internationalized strings.
 * @return array Bulk messages for the `history` post type.
 */
function history_bulk_updated_messages( $bulk_messages, $bulk_counts ) {
	global $post;

	$bulk_messages['history'] = array(
		/* translators: %s: Number of Histories. */
		'updated'   => _n( '%s History updated.', '%s Histories updated.', $bulk_counts['updated'], 'kairos' ),
		'locked'    => ( 1 === $bulk_counts['locked'] ) ? __( '1 History not updated, somebody is editing it.', 'kairos' ) :
						/* translators: %s: Number of Histories. */
						_n( '%s History not updated, somebody is editing it.', '%s Histories not updated, somebody is editing them.', $bulk_counts['locked'], 'kairos' ),
		/* translators: %s: Number of Histories. */
		'deleted'   => _n( '%s History permanently deleted.', '%s Histories permanently deleted.', $bulk_counts['deleted'], 'kairos' ),
		/* translators: %s: Number of Histories. */
		'trashed'   => _n( '%s History moved to the Trash.', '%s Histories moved to the Trash.', $bulk_counts['trashed'], 'kairos' ),
		/* translators: %s: Number of Histories. */
		'untrashed' => _n( '%s History restored from the Trash.', '%s Histories restored from the Trash.', $bulk_counts['untrashed'], 'kairos' ),
	);

	return $bulk_messages;
}
add_filter( 'bulk_post_updated_messages', 'history_bulk_updated_messages', 10, 2 );
