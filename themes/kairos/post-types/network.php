<?php

/**
 * Registers the `network` post type.
 */
function network_init() {
	register_post_type(
		'network',
		array(
			'labels'                => array(
				'name'                  => __( 'Networks', 'kairos' ),
				'singular_name'         => __( 'Network', 'kairos' ),
				'all_items'             => __( 'All Networks', 'kairos' ),
				'archives'              => __( 'Network Archives', 'kairos' ),
				'attributes'            => __( 'Network Attributes', 'kairos' ),
				'insert_into_item'      => __( 'Insert into Network', 'kairos' ),
				'uploaded_to_this_item' => __( 'Uploaded to this Network', 'kairos' ),
				'featured_image'        => _x( 'Featured Image', 'network', 'kairos' ),
				'set_featured_image'    => _x( 'Set featured image', 'network', 'kairos' ),
				'remove_featured_image' => _x( 'Remove featured image', 'network', 'kairos' ),
				'use_featured_image'    => _x( 'Use as featured image', 'network', 'kairos' ),
				'filter_items_list'     => __( 'Filter Networks list', 'kairos' ),
				'items_list_navigation' => __( 'Networks list navigation', 'kairos' ),
				'items_list'            => __( 'Networks list', 'kairos' ),
				'new_item'              => __( 'New Network', 'kairos' ),
				'add_new'               => __( 'Add New', 'kairos' ),
				'add_new_item'          => __( 'Add New Network', 'kairos' ),
				'edit_item'             => __( 'Edit Network', 'kairos' ),
				'view_item'             => __( 'View Network', 'kairos' ),
				'view_items'            => __( 'View Networks', 'kairos' ),
				'search_items'          => __( 'Search Networks', 'kairos' ),
				'not_found'             => __( 'No Networks found', 'kairos' ),
				'not_found_in_trash'    => __( 'No Networks found in trash', 'kairos' ),
				'parent_item_colon'     => __( 'Parent Network:', 'kairos' ),
				'menu_name'             => __( 'Networks', 'kairos' ),
			),
			'public'                => true,
			'hierarchical'          => false,
			'show_ui'               => true,
			'show_in_nav_menus'     => true,
			'supports'              => array( 'title', 'editor' ),
			'has_archive'           => true,
			'rewrite'               => true,
			'query_var'             => true,
			'menu_position'         => null,
			'menu_icon'             => 'dashicons-location-alt',
			'show_in_rest'          => true,
			'rest_base'             => 'network',
			'rest_controller_class' => 'WP_REST_Posts_Controller',
		)
	);

}
add_action( 'init', 'network_init' );

/**
 * Sets the post updated messages for the `network` post type.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `network` post type.
 */
function network_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['network'] = array(
		0  => '', // Unused. Messages start at index 1.
		/* translators: %s: post permalink */
		1  => sprintf( __( 'Network updated. <a target="_blank" href="%s">View Network</a>', 'kairos' ), esc_url( $permalink ) ),
		2  => __( 'Custom field updated.', 'kairos' ),
		3  => __( 'Custom field deleted.', 'kairos' ),
		4  => __( 'Network updated.', 'kairos' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'Network restored to revision from %s', 'kairos' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false, // phpcs:ignore WordPress.Security.NonceVerification.Recommended
		/* translators: %s: post permalink */
		6  => sprintf( __( 'Network published. <a href="%s">View Network</a>', 'kairos' ), esc_url( $permalink ) ),
		7  => __( 'Network saved.', 'kairos' ),
		/* translators: %s: post permalink */
		8  => sprintf( __( 'Network submitted. <a target="_blank" href="%s">Preview Network</a>', 'kairos' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		/* translators: 1: Publish box date format, see https://secure.php.net/date 2: Post permalink */
		9  => sprintf( __( 'Network scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Network</a>', 'kairos' ), date_i18n( __( 'M j, Y @ G:i', 'kairos' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		/* translators: %s: post permalink */
		10 => sprintf( __( 'Network draft updated. <a target="_blank" href="%s">Preview Network</a>', 'kairos' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'network_updated_messages' );

/**
 * Sets the bulk post updated messages for the `network` post type.
 *
 * @param  array $bulk_messages Arrays of messages, each keyed by the corresponding post type. Messages are
 *                              keyed with 'updated', 'locked', 'deleted', 'trashed', and 'untrashed'.
 * @param  int[] $bulk_counts   Array of item counts for each message, used to build internationalized strings.
 * @return array Bulk messages for the `network` post type.
 */
function network_bulk_updated_messages( $bulk_messages, $bulk_counts ) {
	global $post;

	$bulk_messages['network'] = array(
		/* translators: %s: Number of Networks. */
		'updated'   => _n( '%s Network updated.', '%s Networks updated.', $bulk_counts['updated'], 'kairos' ),
		'locked'    => ( 1 === $bulk_counts['locked'] ) ? __( '1 Network not updated, somebody is editing it.', 'kairos' ) :
						/* translators: %s: Number of Networks. */
						_n( '%s Network not updated, somebody is editing it.', '%s Networks not updated, somebody is editing them.', $bulk_counts['locked'], 'kairos' ),
		/* translators: %s: Number of Networks. */
		'deleted'   => _n( '%s Network permanently deleted.', '%s Networks permanently deleted.', $bulk_counts['deleted'], 'kairos' ),
		/* translators: %s: Number of Networks. */
		'trashed'   => _n( '%s Network moved to the Trash.', '%s Networks moved to the Trash.', $bulk_counts['trashed'], 'kairos' ),
		/* translators: %s: Number of Networks. */
		'untrashed' => _n( '%s Network restored from the Trash.', '%s Networks restored from the Trash.', $bulk_counts['untrashed'], 'kairos' ),
	);

	return $bulk_messages;
}
add_filter( 'bulk_post_updated_messages', 'network_bulk_updated_messages', 10, 2 );
