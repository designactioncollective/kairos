<?php

/**
 * Registers the `ppc` post type.
 */
function ppc_init() {
	register_post_type(
		'ppc',
		array(
			'labels'                => array(
				'name'                  => __( 'Poor People’s Campaigns', 'kairos' ),
				'singular_name'         => __( 'Poor People’s Campaign', 'kairos' ),
				'all_items'             => __( 'All Poor People’s Campaigns', 'kairos' ),
				'archives'              => __( 'Poor People’s Campaign Archives', 'kairos' ),
				'attributes'            => __( 'Poor People’s Campaign Attributes', 'kairos' ),
				'insert_into_item'      => __( 'Insert into Poor People’s Campaign', 'kairos' ),
				'uploaded_to_this_item' => __( 'Uploaded to this Poor People’s Campaign', 'kairos' ),
				'featured_image'        => _x( 'Featured Image', 'ppc', 'kairos' ),
				'set_featured_image'    => _x( 'Set featured image', 'ppc', 'kairos' ),
				'remove_featured_image' => _x( 'Remove featured image', 'ppc', 'kairos' ),
				'use_featured_image'    => _x( 'Use as featured image', 'ppc', 'kairos' ),
				'filter_items_list'     => __( 'Filter Poor People’s Campaigns list', 'kairos' ),
				'items_list_navigation' => __( 'Poor People’s Campaigns list navigation', 'kairos' ),
				'items_list'            => __( 'Poor People’s Campaigns list', 'kairos' ),
				'new_item'              => __( 'New Poor People’s Campaign', 'kairos' ),
				'add_new'               => __( 'Add New', 'kairos' ),
				'add_new_item'          => __( 'Add New Poor People’s Campaign', 'kairos' ),
				'edit_item'             => __( 'Edit Poor People’s Campaign', 'kairos' ),
				'view_item'             => __( 'View Poor People’s Campaign', 'kairos' ),
				'view_items'            => __( 'View Poor People’s Campaigns', 'kairos' ),
				'search_items'          => __( 'Search Poor People’s Campaigns', 'kairos' ),
				'not_found'             => __( 'No Poor People’s Campaigns found', 'kairos' ),
				'not_found_in_trash'    => __( 'No Poor People’s Campaigns found in trash', 'kairos' ),
				'parent_item_colon'     => __( 'Parent Poor People’s Campaign:', 'kairos' ),
				'menu_name'             => __( 'Poor People’s Campaigns', 'kairos' ),
			),
			'public'                => true,
			'hierarchical'          => false,
			'show_ui'               => true,
			'show_in_nav_menus'     => true,
			'supports'              => array( 'title', 'editor' ),
			'has_archive'           => true,
			'rewrite'               => true,
			'query_var'             => true,
			'menu_position'         => null,
			'menu_icon'             => 'dashicons-universal-access-alt',
			'show_in_rest'          => true,
			'rest_base'             => 'ppc',
			'rest_controller_class' => 'WP_REST_Posts_Controller',
		)
	);

}
add_action( 'init', 'ppc_init' );

/**
 * Sets the post updated messages for the `ppc` post type.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `ppc` post type.
 */
function ppc_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['ppc'] = array(
		0  => '', // Unused. Messages start at index 1.
		/* translators: %s: post permalink */
		1  => sprintf( __( 'Poor People’s Campaign updated. <a target="_blank" href="%s">View Poor People’s Campaign</a>', 'kairos' ), esc_url( $permalink ) ),
		2  => __( 'Custom field updated.', 'kairos' ),
		3  => __( 'Custom field deleted.', 'kairos' ),
		4  => __( 'Poor People’s Campaign updated.', 'kairos' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'Poor People’s Campaign restored to revision from %s', 'kairos' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false, // phpcs:ignore WordPress.Security.NonceVerification.Recommended
		/* translators: %s: post permalink */
		6  => sprintf( __( 'Poor People’s Campaign published. <a href="%s">View Poor People’s Campaign</a>', 'kairos' ), esc_url( $permalink ) ),
		7  => __( 'Poor People’s Campaign saved.', 'kairos' ),
		/* translators: %s: post permalink */
		8  => sprintf( __( 'Poor People’s Campaign submitted. <a target="_blank" href="%s">Preview Poor People’s Campaign</a>', 'kairos' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		/* translators: 1: Publish box date format, see https://secure.php.net/date 2: Post permalink */
		9  => sprintf( __( 'Poor People’s Campaign scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Poor People’s Campaign</a>', 'kairos' ), date_i18n( __( 'M j, Y @ G:i', 'kairos' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		/* translators: %s: post permalink */
		10 => sprintf( __( 'Poor People’s Campaign draft updated. <a target="_blank" href="%s">Preview Poor People’s Campaign</a>', 'kairos' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'ppc_updated_messages' );

/**
 * Sets the bulk post updated messages for the `ppc` post type.
 *
 * @param  array $bulk_messages Arrays of messages, each keyed by the corresponding post type. Messages are
 *                              keyed with 'updated', 'locked', 'deleted', 'trashed', and 'untrashed'.
 * @param  int[] $bulk_counts   Array of item counts for each message, used to build internationalized strings.
 * @return array Bulk messages for the `ppc` post type.
 */
function ppc_bulk_updated_messages( $bulk_messages, $bulk_counts ) {
	global $post;

	$bulk_messages['ppc'] = array(
		/* translators: %s: Number of Poor People’s Campaigns. */
		'updated'   => _n( '%s Poor People’s Campaign updated.', '%s Poor People’s Campaigns updated.', $bulk_counts['updated'], 'kairos' ),
		'locked'    => ( 1 === $bulk_counts['locked'] ) ? __( '1 Poor People’s Campaign not updated, somebody is editing it.', 'kairos' ) :
						/* translators: %s: Number of Poor People’s Campaigns. */
						_n( '%s Poor People’s Campaign not updated, somebody is editing it.', '%s Poor People’s Campaigns not updated, somebody is editing them.', $bulk_counts['locked'], 'kairos' ),
		/* translators: %s: Number of Poor People’s Campaigns. */
		'deleted'   => _n( '%s Poor People’s Campaign permanently deleted.', '%s Poor People’s Campaigns permanently deleted.', $bulk_counts['deleted'], 'kairos' ),
		/* translators: %s: Number of Poor People’s Campaigns. */
		'trashed'   => _n( '%s Poor People’s Campaign moved to the Trash.', '%s Poor People’s Campaigns moved to the Trash.', $bulk_counts['trashed'], 'kairos' ),
		/* translators: %s: Number of Poor People’s Campaigns. */
		'untrashed' => _n( '%s Poor People’s Campaign restored from the Trash.', '%s Poor People’s Campaigns restored from the Trash.', $bulk_counts['untrashed'], 'kairos' ),
	);

	return $bulk_messages;
}
add_filter( 'bulk_post_updated_messages', 'ppc_bulk_updated_messages', 10, 2 );
