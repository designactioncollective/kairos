<?php
/**
 * Plugin Name: Design Action plugins
 * Description: Adds section to Customizer for the site logo. This appears under "Site Identity" section.
 * Author: Design Action Collective
*/

add_action('after_setup_theme', function () {
    /**
     * Let client add custom logo
     */
    add_theme_support( 'custom-logo' );
}, 30);
