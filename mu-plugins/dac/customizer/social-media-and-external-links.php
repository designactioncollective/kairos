<?php
/**
 * Plugin Name: Design Action plugins
 * Description: Adds section to Customizer for social media urls and external links
 * Author: Design Action Collective
*/

/**
* Add Social Media Links
*/
add_action('customize_register', function (\WP_Customize_Manager $wp_customize) {
    $wp_customize->add_section('social_media_links', array(
        'title' =>       'Social Media and External Links',
        'description' => '',
        'priority' =>    120,
    ));

    $wp_customize->add_setting( 'facebook_link' );
    $wp_customize->add_setting( 'twitter_link' );
    $wp_customize->add_setting( 'instagram_link' );
    $wp_customize->add_setting( 'donation_link' );
    $wp_customize->add_setting( 'donation_title' );
    $wp_customize->add_setting( 'store_link' );
    $wp_customize->add_setting( 'store_title' );
    $wp_customize->add_setting( 'sign_up_link' );
    $wp_customize->add_setting( 'sign_up_title' );

    $wp_customize->add_control( 'facebook_link', array(
        'type' => 'url',
        'section' => 'social_media_links',
        'settings' => 'facebook_link',
        'label' => __( 'Facebook Link' ),
        'description' => __( 'Include a link to your facebook page.' ),
        )
    );

    $wp_customize->add_control( 'twitter_link', array(
        'type' => 'url',
        'section' => 'social_media_links',
        'settings' => 'twitter_link',
        'label' => __( 'Twitter Link' ),
        'description' => __( 'Include a link to your twitter page.' ),
        )
    );

    $wp_customize->add_control( 'instagram_link', array(
        'type' => 'url',
        'section' => 'social_media_links',
        'settings' => 'instagram_link',
        'label' => __( 'Instagram Link' ),
        'description' => __( 'Include a link to your instagram page.' ),
        )
    );

    $wp_customize->add_control( 'donation_link', array(
        'type' => 'url',
        'section' => 'social_media_links',
        'settings' => 'donation_link',
        'label' => __( 'Link to donation page' ),
        'description' => __( 'Include a link to your donation page.' ),
        )
    );

    $wp_customize->add_control( 'donation_title', array(
        'type' => 'textarea',
        'section' => 'social_media_links',
        'settings' => 'donation_title',
        'label' => __( 'Title of donation page link' ),
        'description' => __( 'Title for the donation page.' ),
        )
    );

    $wp_customize->add_control( 'store_link', array(
        'type' => 'url',
        'section' => 'social_media_links',
        'settings' => 'store_link',
        'label' => __( 'Link to Store' ),
        'description' => __( 'Include a link to your external store page.' ),
        )
    );

    $wp_customize->add_control( 'store_title', array(
        'type' => 'input',
        'section' => 'social_media_links',
        'settings' => 'store_title',
        'label' => __( 'Title of donation page link' ),
        'description' => __( 'Title for the store page.' ),
        )
    );

    $wp_customize->add_control( 'sign_up_link', array(
        'type' => 'url',
        'section' => 'social_media_links',
        'settings' => 'sign_up_link',
        'label' => __( 'Link to Sign Up page' ),
        'description' => __( 'Link to your sign up page.' ),
        )
    );

    $wp_customize->add_control( 'sign_up_title', array(
        'type' => 'input',
        'section' => 'social_media_links',
        'settings' => 'sign_up_title',
        'label' => __( 'Title of sign up link' ),
        // 'description' => __( 'Title for the sign up link.' ),
        )
    );
});
