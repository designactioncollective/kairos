<?php

/**
 * Registers the `culture` post type.
 */
function culture_init() {
	register_post_type(
		'culture',
		array(
			'labels'                => array(
				'name'                  => __( 'Culture &amp; Communications', 'kairos' ),
				'singular_name'         => __( 'Culture &amp; Communications', 'kairos' ),
				'all_items'             => __( 'All Culture and Communications', 'kairos' ),
				'archives'              => __( 'Culture &amp; Communications', 'kairos' ),
				'attributes'            => __( 'Culture and Communications Attributes', 'kairos' ),
				'insert_into_item'      => __( 'Insert into Culture and Communications', 'kairos' ),
				'uploaded_to_this_item' => __( 'Uploaded to this Culture and Communications', 'kairos' ),
				'featured_image'        => _x( 'Featured Image', 'culture', 'kairos' ),
				'set_featured_image'    => _x( 'Set featured image', 'culture', 'kairos' ),
				'remove_featured_image' => _x( 'Remove featured image', 'culture', 'kairos' ),
				'use_featured_image'    => _x( 'Use as featured image', 'culture', 'kairos' ),
				'filter_items_list'     => __( 'Filter Culture and Communications list', 'kairos' ),
				'items_list_navigation' => __( 'Culture and Communications list navigation', 'kairos' ),
				'items_list'            => __( 'Culture and Communications list', 'kairos' ),
				'new_item'              => __( 'New Culture and Communications', 'kairos' ),
				'add_new'               => __( 'Add New', 'kairos' ),
				'add_new_item'          => __( 'Add New Culture and Communications', 'kairos' ),
				'edit_item'             => __( 'Edit Culture and Communications', 'kairos' ),
				'view_item'             => __( 'View Culture and Communications', 'kairos' ),
				'view_items'            => __( 'View Culture and Communications', 'kairos' ),
				'search_items'          => __( 'Search Culture and Communications', 'kairos' ),
				'not_found'             => __( 'No Culture and Communications found', 'kairos' ),
				'not_found_in_trash'    => __( 'No Culture and Communications found in trash', 'kairos' ),
				'parent_item_colon'     => __( 'Parent Culture and Communications:', 'kairos' ),
				'menu_name'             => __( 'Culture and Communications', 'kairos' ),
			),
			'public'                => true,
			'hierarchical'          => false,
			'show_ui'               => true,
			'show_in_nav_menus'     => true,
			'supports'              => array( 'title', 'editor' ),
			'has_archive'           => true,
			'rewrite'               => true,
			'query_var'             => true,
			'menu_position'         => null,
			'menu_icon'             => 'dashicons-album',
			'show_in_rest'          => true,
			'rest_base'             => 'culture',
			'rest_controller_class' => 'WP_REST_Posts_Controller',
		)
	);

}
add_action( 'init', 'culture_init' );

/**
 * Sets the post updated messages for the `culture` post type.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `culture` post type.
 */
function culture_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['culture'] = array(
		0  => '', // Unused. Messages start at index 1.
		/* translators: %s: post permalink */
		1  => sprintf( __( 'Culture and Communications updated. <a target="_blank" href="%s">View Culture and Communications</a>', 'kairos' ), esc_url( $permalink ) ),
		2  => __( 'Custom field updated.', 'kairos' ),
		3  => __( 'Custom field deleted.', 'kairos' ),
		4  => __( 'Culture and Communications updated.', 'kairos' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'Culture and Communications restored to revision from %s', 'kairos' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false, // phpcs:ignore WordPress.Security.NonceVerification.Recommended
		/* translators: %s: post permalink */
		6  => sprintf( __( 'Culture and Communications published. <a href="%s">View Culture and Communications</a>', 'kairos' ), esc_url( $permalink ) ),
		7  => __( 'Culture and Communications saved.', 'kairos' ),
		/* translators: %s: post permalink */
		8  => sprintf( __( 'Culture and Communications submitted. <a target="_blank" href="%s">Preview Culture and Communications</a>', 'kairos' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		/* translators: 1: Publish box date format, see https://secure.php.net/date 2: Post permalink */
		9  => sprintf( __( 'Culture and Communications scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Culture and Communications</a>', 'kairos' ), date_i18n( __( 'M j, Y @ G:i', 'kairos' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		/* translators: %s: post permalink */
		10 => sprintf( __( 'Culture and Communications draft updated. <a target="_blank" href="%s">Preview Culture and Communications</a>', 'kairos' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'culture_updated_messages' );

/**
 * Sets the bulk post updated messages for the `culture` post type.
 *
 * @param  array $bulk_messages Arrays of messages, each keyed by the corresponding post type. Messages are
 *                              keyed with 'updated', 'locked', 'deleted', 'trashed', and 'untrashed'.
 * @param  int[] $bulk_counts   Array of item counts for each message, used to build internationalized strings.
 * @return array Bulk messages for the `culture` post type.
 */
function culture_bulk_updated_messages( $bulk_messages, $bulk_counts ) {
	global $post;

	$bulk_messages['culture'] = array(
		/* translators: %s: Number of Culture and Communications. */
		'updated'   => _n( '%s Culture and Communications updated.', '%s Culture and Communications updated.', $bulk_counts['updated'], 'kairos' ),
		'locked'    => ( 1 === $bulk_counts['locked'] ) ? __( '1 Culture and Communications not updated, somebody is editing it.', 'kairos' ) :
						/* translators: %s: Number of Culture and Communications. */
						_n( '%s Culture and Communications not updated, somebody is editing it.', '%s Culture and Communications not updated, somebody is editing them.', $bulk_counts['locked'], 'kairos' ),
		/* translators: %s: Number of Culture and Communications. */
		'deleted'   => _n( '%s Culture and Communications permanently deleted.', '%s Culture and Communications permanently deleted.', $bulk_counts['deleted'], 'kairos' ),
		/* translators: %s: Number of Culture and Communications. */
		'trashed'   => _n( '%s Culture and Communications moved to the Trash.', '%s Culture and Communications moved to the Trash.', $bulk_counts['trashed'], 'kairos' ),
		/* translators: %s: Number of Culture and Communications. */
		'untrashed' => _n( '%s Culture and Communications restored from the Trash.', '%s Culture and Communications restored from the Trash.', $bulk_counts['untrashed'], 'kairos' ),
	);

	return $bulk_messages;
}
add_filter( 'bulk_post_updated_messages', 'culture_bulk_updated_messages', 10, 2 );
