<?php

/**
 * Registers the `staff` post type.
 */
function staff_init() {
	register_post_type( 'staff', array(
		'labels'                => array(
			'name'                  => __( 'Staff', 'kairos' ),
			'singular_name'         => __( 'Staff', 'kairos' ),
			'all_items'             => __( 'All Staff', 'kairos' ),
			'archives'              => __( 'Staff', 'kairos' ),
			'attributes'            => __( 'Staff Attributes', 'kairos' ),
			'insert_into_item'      => __( 'Insert into Staff', 'kairos' ),
			'uploaded_to_this_item' => __( 'Uploaded to this Staff', 'kairos' ),
			'featured_image'        => _x( 'Featured Image', 'staff', 'kairos' ),
			'set_featured_image'    => _x( 'Set featured image', 'staff', 'kairos' ),
			'remove_featured_image' => _x( 'Remove featured image', 'staff', 'kairos' ),
			'use_featured_image'    => _x( 'Use as featured image', 'staff', 'kairos' ),
			'filter_items_list'     => __( 'Filter Staff list', 'kairos' ),
			'items_list_navigation' => __( 'Staff list navigation', 'kairos' ),
			'items_list'            => __( 'Staff list', 'kairos' ),
			'new_item'              => __( 'New Staff', 'kairos' ),
			'add_new'               => __( 'Add New', 'kairos' ),
			'add_new_item'          => __( 'Add New Staff', 'kairos' ),
			'edit_item'             => __( 'Edit Staff', 'kairos' ),
			'view_item'             => __( 'View Staff', 'kairos' ),
			'view_items'            => __( 'View Staff', 'kairos' ),
			'search_items'          => __( 'Search Staff', 'kairos' ),
			'not_found'             => __( 'No Staff found', 'kairos' ),
			'not_found_in_trash'    => __( 'No Staff found in trash', 'kairos' ),
			'parent_item_colon'     => __( 'Parent Staff:', 'kairos' ),
			'menu_name'             => __( 'Staff', 'kairos' ),
		),
		'public'                => true,
		'hierarchical'          => false,
		'show_ui'               => true,
		'show_in_nav_menus'     => true,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'has_archive'           => true,
		'rewrite'               => true,
		'query_var'             => true,
		'menu_position'         => null,
		'menu_icon'             => 'dashicons-businesswoman',
		'show_in_rest'          => true,
		'rest_base'             => 'staff',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'staff_init' );

/**
 * Sets the post updated messages for the `staff` post type.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `staff` post type.
 */
function staff_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['staff'] = array(
		0  => '', // Unused. Messages start at index 1.
		/* translators: %s: post permalink */
		1  => sprintf( __( 'Staff updated. <a target="_blank" href="%s">View Staff</a>', 'kairos' ), esc_url( $permalink ) ),
		2  => __( 'Custom field updated.', 'kairos' ),
		3  => __( 'Custom field deleted.', 'kairos' ),
		4  => __( 'Staff updated.', 'kairos' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'Staff restored to revision from %s', 'kairos' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		/* translators: %s: post permalink */
		6  => sprintf( __( 'Staff published. <a href="%s">View Staff</a>', 'kairos' ), esc_url( $permalink ) ),
		7  => __( 'Staff saved.', 'kairos' ),
		/* translators: %s: post permalink */
		8  => sprintf( __( 'Staff submitted. <a target="_blank" href="%s">Preview Staff</a>', 'kairos' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		/* translators: 1: Publish box date format, see https://secure.php.net/date 2: Post permalink */
		9  => sprintf( __( 'Staff scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Staff</a>', 'kairos' ),
		date_i18n( __( 'M j, Y @ G:i', 'kairos' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		/* translators: %s: post permalink */
		10 => sprintf( __( 'Staff draft updated. <a target="_blank" href="%s">Preview Staff</a>', 'kairos' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'staff_updated_messages' );
