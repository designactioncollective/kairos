<?php

/**
 * Registers the `jobs` post type.
 */
function jobs_init() {
	register_post_type( 'jobs', array(
		'labels'                => array(
			'name'                  => __( 'Jobs & Internships', 'kairos' ),
			'singular_name'         => __( 'Jobs', 'kairos' ),
			'all_items'             => __( 'All Jobs', 'kairos' ),
			'archives'              => __( 'Jobs & Internships', 'kairos' ),
			'attributes'            => __( 'Jobs Attributes', 'kairos' ),
			'insert_into_item'      => __( 'Insert into Jobs', 'kairos' ),
			'uploaded_to_this_item' => __( 'Uploaded to this Jobs', 'kairos' ),
			'featured_image'        => _x( 'Featured Image', 'jobs', 'kairos' ),
			'set_featured_image'    => _x( 'Set featured image', 'jobs', 'kairos' ),
			'remove_featured_image' => _x( 'Remove featured image', 'jobs', 'kairos' ),
			'use_featured_image'    => _x( 'Use as featured image', 'jobs', 'kairos' ),
			'filter_items_list'     => __( 'Filter Jobs list', 'kairos' ),
			'items_list_navigation' => __( 'Jobs list navigation', 'kairos' ),
			'items_list'            => __( 'Jobs list', 'kairos' ),
			'new_item'              => __( 'New Jobs', 'kairos' ),
			'add_new'               => __( 'Add New', 'kairos' ),
			'add_new_item'          => __( 'Add New Jobs', 'kairos' ),
			'edit_item'             => __( 'Edit Jobs', 'kairos' ),
			'view_item'             => __( 'View Jobs', 'kairos' ),
			'view_items'            => __( 'View Jobs', 'kairos' ),
			'search_items'          => __( 'Search Jobs', 'kairos' ),
			'not_found'             => __( 'No Jobs found', 'kairos' ),
			'not_found_in_trash'    => __( 'No Jobs found in trash', 'kairos' ),
			'parent_item_colon'     => __( 'Parent Jobs:', 'kairos' ),
			'menu_name'             => __( 'Jobs', 'kairos' ),
		),
		'public'                => true,
		'hierarchical'          => false,
		'show_ui'               => true,
		'show_in_nav_menus'     => true,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'has_archive'           => true,
		'rewrite'               => true,
		'query_var'             => true,
		'menu_position'         => null,
		'menu_icon'             => 'dashicons-id-alt',
		'show_in_rest'          => false,
		'rest_base'             => 'jobs',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'jobs_init' );

/**
 * Sets the post updated messages for the `jobs` post type.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `jobs` post type.
 */
function jobs_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['jobs'] = array(
		0  => '', // Unused. Messages start at index 1.
		/* translators: %s: post permalink */
		1  => sprintf( __( 'Jobs updated. <a target="_blank" href="%s">View Jobs</a>', 'kairos' ), esc_url( $permalink ) ),
		2  => __( 'Custom field updated.', 'kairos' ),
		3  => __( 'Custom field deleted.', 'kairos' ),
		4  => __( 'Jobs updated.', 'kairos' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'Jobs restored to revision from %s', 'kairos' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		/* translators: %s: post permalink */
		6  => sprintf( __( 'Jobs published. <a href="%s">View Jobs</a>', 'kairos' ), esc_url( $permalink ) ),
		7  => __( 'Jobs saved.', 'kairos' ),
		/* translators: %s: post permalink */
		8  => sprintf( __( 'Jobs submitted. <a target="_blank" href="%s">Preview Jobs</a>', 'kairos' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		/* translators: 1: Publish box date format, see https://secure.php.net/date 2: Post permalink */
		9  => sprintf( __( 'Jobs scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Jobs</a>', 'kairos' ),
		date_i18n( __( 'M j, Y @ G:i', 'kairos' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		/* translators: %s: post permalink */
		10 => sprintf( __( 'Jobs draft updated. <a target="_blank" href="%s">Preview Jobs</a>', 'kairos' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'jobs_updated_messages' );
