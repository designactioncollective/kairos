<?php

/**
 * Registers the `resources_cpt` post type.
 */
function resources_cpt_init() {
	register_post_type(
		'resources_cpt',
		array(
			'labels'                => array(
				'name'                  => __( 'Resources', 'kairos' ),
				'singular_name'         => __( 'Resources', 'kairos' ),
				'all_items'             => __( 'All Resources', 'kairos' ),
				'archives'              => __( 'Resources', 'kairos' ),
				'attributes'            => __( 'Resources Attributes', 'kairos' ),
				'insert_into_item'      => __( 'Insert into Resources', 'kairos' ),
				'uploaded_to_this_item' => __( 'Uploaded to this Resources', 'kairos' ),
				'featured_image'        => __( 'Featured Image', 'resources_cpt', 'kairos' ),
				'set_featured_image'    => __( 'Set featured image', 'resources_cpt', 'kairos' ),
				'remove_featured_image' => __( 'Remove featured image', 'resources_cpt', 'kairos' ),
				'use_featured_image'    => __( 'Use as featured image', 'resources_cpt', 'kairos' ),
				'filter_items_list'     => __( 'Filter Resources list', 'kairos' ),
				'items_list_navigation' => __( 'Resources list navigation', 'kairos' ),
				'items_list'            => __( 'Resources list', 'kairos' ),
				'new_item'              => __( 'New Resources', 'kairos' ),
				'add_new'               => __( 'Add New', 'kairos' ),
				'add_new_item'          => __( 'Add New Resources', 'kairos' ),
				'edit_item'             => __( 'Edit Resources', 'kairos' ),
				'view_item'             => __( 'View Resources', 'kairos' ),
				'view_items'            => __( 'View Resources', 'kairos' ),
				'search_items'          => __( 'Search Resources', 'kairos' ),
				'not_found'             => __( 'No Resources found', 'kairos' ),
				'not_found_in_trash'    => __( 'No Resources found in trash', 'kairos' ),
				'parent_item_colon'     => __( 'Parent Resources:', 'kairos' ),
				'menu_name'             => __( 'Resources', 'kairos' ),
			),
			'public'                => true,
			'hierarchical'          => false,
			'show_ui'               => true,
			'show_in_nav_menus'     => true,
			'supports'              => array( 'title', 'editor', 'thumbnail' ),
			'has_archive'           => true,
			'rewrite'               => true,
			'query_var'             => true,
			'menu_position'         => null,
			'menu_icon'             => 'dashicons-bell',
			'show_in_rest'          => true,
			'rest_base'             => 'resources_cpt',
			'rest_controller_class' => 'WP_REST_Posts_Controller',
		)
	);

}
add_action( 'init', 'resources_cpt_init' );

/**
 * Sets the post updated messages for the `resources_cpt` post type.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `resources_cpt` post type.
 */
function resources_cpt_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['resources_cpt'] = array(
		0  => '', // Unused. Messages start at index 1.
		/* translators: %s: post permalink */
		1  => sprintf( __( 'Resources updated. <a target="_blank" href="%s">View Resources</a>', 'kairos' ), esc_url( $permalink ) ),
		2  => __( 'Custom field updated.', 'kairos' ),
		3  => __( 'Custom field deleted.', 'kairos' ),
		4  => __( 'Resources updated.', 'kairos' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'Resources restored to revision from %s', 'kairos' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false, // phpcs:ignore WordPress.Security.NonceVerification.Recommended
		/* translators: %s: post permalink */
		6  => sprintf( __( 'Resources published. <a href="%s">View Resources</a>', 'kairos' ), esc_url( $permalink ) ),
		7  => __( 'Resources saved.', 'kairos' ),
		/* translators: %s: post permalink */
		8  => sprintf( __( 'Resources submitted. <a target="_blank" href="%s">Preview Resources</a>', 'kairos' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		/* translators: 1: Publish box date format, see https://secure.php.net/date 2: Post permalink */
		9  => sprintf( __( 'Resources scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Resources</a>', 'kairos' ), date_i18n( __( 'M j, Y @ G:i', 'kairos' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		/* translators: %s: post permalink */
		10 => sprintf( __( 'Resources draft updated. <a target="_blank" href="%s">Preview Resources</a>', 'kairos' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'resources_cpt_updated_messages' );

/**
 * Sets the bulk post updated messages for the `resources_cpt` post type.
 *
 * @param  array $bulk_messages Arrays of messages, each keyed by the corresponding post type. Messages are
 *                              keyed with 'updated', 'locked', 'deleted', 'trashed', and 'untrashed'.
 * @param  int[] $bulk_counts   Array of item counts for each message, used to build internationalized strings.
 * @return array Bulk messages for the `resources_cpt` post type.
 */
function resources_cpt_bulk_updated_messages( $bulk_messages, $bulk_counts ) {
	global $post;

	$bulk_messages['resources_cpt'] = array(
		/* translators: %s: Number of Resources. */
		'updated'   => _n( '%s Resources updated.', '%s Resources updated.', $bulk_counts['updated'], 'kairos' ),
		'locked'    => ( 1 === $bulk_counts['locked'] ) ? __( '1 Resources not updated, somebody is editing it.', 'kairos' ) :
						/* translators: %s: Number of Resources. */
						_n( '%s Resources not updated, somebody is editing it.', '%s Resources not updated, somebody is editing them.', $bulk_counts['locked'], 'kairos' ),
		/* translators: %s: Number of Resources. */
		'deleted'   => _n( '%s Resources permanently deleted.', '%s Resources permanently deleted.', $bulk_counts['deleted'], 'kairos' ),
		/* translators: %s: Number of Resources. */
		'trashed'   => _n( '%s Resources moved to the Trash.', '%s Resources moved to the Trash.', $bulk_counts['trashed'], 'kairos' ),
		/* translators: %s: Number of Resources. */
		'untrashed' => _n( '%s Resources restored from the Trash.', '%s Resources restored from the Trash.', $bulk_counts['untrashed'], 'kairos' ),
	);

	return $bulk_messages;
}
add_filter( 'bulk_post_updated_messages', 'resources_cpt_bulk_updated_messages', 10, 2 );
