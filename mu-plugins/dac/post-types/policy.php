<?php

/**
 * Registers the `policy` post type.
 */
function policy_init() {
	register_post_type(
		'policy',
		array(
			'labels'                => array(
				'name'                  => __( 'Policy', 'kairos' ),
				'singular_name'         => __( 'Policy', 'kairos' ),
				'all_items'             => __( 'All Policies', 'kairos' ),
				'archives'              => __( 'Policy', 'kairos' ),
				'attributes'            => __( 'Policy Attributes', 'kairos' ),
				'insert_into_item'      => __( 'Insert into Policy', 'kairos' ),
				'uploaded_to_this_item' => __( 'Uploaded to this Policy', 'kairos' ),
				'featured_image'        => _x( 'Featured Image', 'policy', 'kairos' ),
				'set_featured_image'    => _x( 'Set featured image', 'policy', 'kairos' ),
				'remove_featured_image' => _x( 'Remove featured image', 'policy', 'kairos' ),
				'use_featured_image'    => _x( 'Use as featured image', 'policy', 'kairos' ),
				'filter_items_list'     => __( 'Filter Policies list', 'kairos' ),
				'items_list_navigation' => __( 'Policies list navigation', 'kairos' ),
				'items_list'            => __( 'Policies list', 'kairos' ),
				'new_item'              => __( 'New Policy', 'kairos' ),
				'add_new'               => __( 'Add New', 'kairos' ),
				'add_new_item'          => __( 'Add New Policy', 'kairos' ),
				'edit_item'             => __( 'Edit Policy', 'kairos' ),
				'view_item'             => __( 'View Policy', 'kairos' ),
				'view_items'            => __( 'View Policies', 'kairos' ),
				'search_items'          => __( 'Search Policies', 'kairos' ),
				'not_found'             => __( 'No Policies found', 'kairos' ),
				'not_found_in_trash'    => __( 'No Policies found in trash', 'kairos' ),
				'parent_item_colon'     => __( 'Parent Policy:', 'kairos' ),
				'menu_name'             => __( 'Policies', 'kairos' ),
			),
			'public'                => true,
			'hierarchical'          => false,
			'show_ui'               => true,
			'show_in_nav_menus'     => true,
			'supports'              => array( 'title', 'editor' ),
			'has_archive'           => true,
			'rewrite'               => true,
			'query_var'             => true,
			'menu_position'         => null,
			'menu_icon'             => 'dashicons-text-page',
			'show_in_rest'          => true,
			'rest_base'             => 'policy',
			'rest_controller_class' => 'WP_REST_Posts_Controller',
		)
	);
}
add_action( 'init', 'policy_init' );

/**
 * Sets the post updated messages for the `policy` post type.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `policy` post type.
 */
function policy_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['policy'] = array(
		0  => '', // Unused. Messages start at index 1.
		/* translators: %s: post permalink */
		1  => sprintf( __( 'Policy updated. <a target="_blank" href="%s">View Policy</a>', 'kairos' ), esc_url( $permalink ) ),
		2  => __( 'Custom field updated.', 'kairos' ),
		3  => __( 'Custom field deleted.', 'kairos' ),
		4  => __( 'Policy updated.', 'kairos' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'Policy restored to revision from %s', 'kairos' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false, // phpcs:ignore WordPress.Security.NonceVerification.Recommended
		/* translators: %s: post permalink */
		6  => sprintf( __( 'Policy published. <a href="%s">View Policy</a>', 'kairos' ), esc_url( $permalink ) ),
		7  => __( 'Policy saved.', 'kairos' ),
		/* translators: %s: post permalink */
		8  => sprintf( __( 'Policy submitted. <a target="_blank" href="%s">Preview Policy</a>', 'kairos' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		/* translators: 1: Publish box date format, see https://secure.php.net/date 2: Post permalink */
		9  => sprintf( __( 'Policy scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Policy</a>', 'kairos' ), date_i18n( __( 'M j, Y @ G:i', 'kairos' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		/* translators: %s: post permalink */
		10 => sprintf( __( 'Policy draft updated. <a target="_blank" href="%s">Preview Policy</a>', 'kairos' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'policy_updated_messages' );

/**
 * Sets the bulk post updated messages for the `policy` post type.
 *
 * @param  array $bulk_messages Arrays of messages, each keyed by the corresponding post type. Messages are
 *                              keyed with 'updated', 'locked', 'deleted', 'trashed', and 'untrashed'.
 * @param  int[] $bulk_counts   Array of item counts for each message, used to build internationalized strings.
 * @return array Bulk messages for the `policy` post type.
 */
function policy_bulk_updated_messages( $bulk_messages, $bulk_counts ) {
	global $post;

	$bulk_messages['policy'] = array(
		/* translators: %s: Number of Policies. */
		'updated'   => _n( '%s Policy updated.', '%s Policies updated.', $bulk_counts['updated'], 'kairos' ),
		'locked'    => ( 1 === $bulk_counts['locked'] ) ? __( '1 Policy not updated, somebody is editing it.', 'kairos' ) :
						/* translators: %s: Number of Policies. */
						_n( '%s Policy not updated, somebody is editing it.', '%s Policies not updated, somebody is editing them.', $bulk_counts['locked'], 'kairos' ),
		/* translators: %s: Number of Policies. */
		'deleted'   => _n( '%s Policy permanently deleted.', '%s Policies permanently deleted.', $bulk_counts['deleted'], 'kairos' ),
		/* translators: %s: Number of Policies. */
		'trashed'   => _n( '%s Policy moved to the Trash.', '%s Policies moved to the Trash.', $bulk_counts['trashed'], 'kairos' ),
		/* translators: %s: Number of Policies. */
		'untrashed' => _n( '%s Policy restored from the Trash.', '%s Policies restored from the Trash.', $bulk_counts['untrashed'], 'kairos' ),
	);

	return $bulk_messages;
}
add_filter( 'bulk_post_updated_messages', 'policy_bulk_updated_messages', 10, 2 );
