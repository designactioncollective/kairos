<?php

/**
 * Registers the `resource-type` taxonomy,
 * for use with 'resources_cpt'.
 */
function resource_type_init() {
	register_taxonomy( 'resource-type', array( 'resources_cpt' ), array(
		'hierarchical'      => true,
		'public'            => true,
		'show_in_nav_menus' => true,
		'show_ui'           => true,
		'show_admin_column' => false,
		'query_var'         => true,
		'rewrite'           => true,
		'capabilities'      => array(
			'manage_terms'  => 'edit_posts',
			'edit_terms'    => 'edit_posts',
			'delete_terms'  => 'edit_posts',
			'assign_terms'  => 'edit_posts',
		),
		'labels'            => array(
			'name'                       => __( 'Types', 'kairos' ),
			'singular_name'              => _x( 'Type', 'taxonomy general name', 'kairos' ),
			'search_items'               => __( 'Search Types', 'kairos' ),
			'popular_items'              => __( 'Popular Types', 'kairos' ),
			'all_items'                  => __( 'All Types', 'kairos' ),
			'parent_item'                => __( 'Parent Type', 'kairos' ),
			'parent_item_colon'          => __( 'Parent Type:', 'kairos' ),
			'edit_item'                  => __( 'Edit Type', 'kairos' ),
			'update_item'                => __( 'Update Type', 'kairos' ),
			'view_item'                  => __( 'View Type', 'kairos' ),
			'add_new_item'               => __( 'Add New Type', 'kairos' ),
			'new_item_name'              => __( 'New Type', 'kairos' ),
			'separate_items_with_commas' => __( 'Separate Types with commas', 'kairos' ),
			'add_or_remove_items'        => __( 'Add or remove Types', 'kairos' ),
			'choose_from_most_used'      => __( 'Choose from the most used Types', 'kairos' ),
			'not_found'                  => __( 'No Types found.', 'kairos' ),
			'no_terms'                   => __( 'No Types', 'kairos' ),
			'menu_name'                  => __( 'Types', 'kairos' ),
			'items_list_navigation'      => __( 'Types list navigation', 'kairos' ),
			'items_list'                 => __( 'Types list', 'kairos' ),
			'most_used'                  => _x( 'Most Used', 'Type', 'kairos' ),
			'back_to_items'              => __( '&larr; Back to Types', 'kairos' ),
		),
		'show_in_rest'      => true,
		'rest_base'         => 'resource_type_type',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
	) );

}
add_action( 'init', 'resource_type_init' );

/**
 * Sets the post updated messages for the `Type` taxonomy.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `Type` taxonomy.
 */
function resource_type_updated_messages( $messages ) {

	$messages['resource_type_type'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => __( 'Type added.', 'kairos' ),
		2 => __( 'Type deleted.', 'kairos' ),
		3 => __( 'Type updated.', 'kairos' ),
		4 => __( 'Type not added.', 'kairos' ),
		5 => __( 'Type not updated.', 'kairos' ),
		6 => __( 'Types deleted.', 'kairos' ),
	);

	return $messages;
}
add_filter( 'term_updated_messages', 'resource_type_updated_messages' );
