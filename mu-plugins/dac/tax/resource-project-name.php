<?php

/**
 * Registers the `resource-project-name` taxonomy,
 * for use with 'resources_cpt', 'projects_cpt'.
 */
function resource_project_type_init() {
	register_taxonomy( 'resource-project-name', array( 'resources_cpt', 'projects_cpt' ), array(
		'hierarchical'      => true,
		'public'            => true,
		'show_in_nav_menus' => true,
		'show_ui'           => true,
		'show_admin_column' => false,
		'query_var'         => true,
		'rewrite'           => array('slug' => 'projects', 'with_front' => false),
		'capabilities'      => array(
			'manage_terms'  => 'edit_posts',
			'edit_terms'    => 'edit_posts',
			'delete_terms'  => 'edit_posts',
			'assign_terms'  => 'edit_posts',
		),
		'labels'            => array(
			'name'                       => __( 'Project Names', 'kairos' ),
			'singular_name'              => _x( 'Project Name', 'taxonomy general name', 'kairos' ),
			'search_items'               => __( 'Search Projects', 'kairos' ),
			'popular_items'              => __( 'Popular Projects', 'kairos' ),
			'all_items'                  => __( 'All Projects', 'kairos' ),
			'parent_item'                => __( 'Parent Project', 'kairos' ),
			'parent_item_colon'          => __( 'Parent Project:', 'kairos' ),
			'edit_item'                  => __( 'Edit Project', 'kairos' ),
			'update_item'                => __( 'Update Project', 'kairos' ),
			'view_item'                  => __( 'View Project', 'kairos' ),
			'add_new_item'               => __( 'Add New Project', 'kairos' ),
			'new_item_name'              => __( 'New Project', 'kairos' ),
			'separate_items_with_commas' => __( 'Separate Projects with commas', 'kairos' ),
			'add_or_remove_items'        => __( 'Add or remove Projects', 'kairos' ),
			'choose_from_most_used'      => __( 'Choose from the most used Projects', 'kairos' ),
			'not_found'                  => __( 'No Projects found.', 'kairos' ),
			'no_terms'                   => __( 'No Projects', 'kairos' ),
			'menu_name'                  => __( 'Project Names', 'kairos' ),
			'items_list_navigation'      => __( 'Projects list navigation', 'kairos' ),
			'items_list'                 => __( 'Projects list', 'kairos' ),
			'most_used'                  => _x( 'Most Used', 'Project', 'kairos' ),
			'back_to_items'              => __( '&larr; Back to Projects', 'kairos' ),
		),
		'show_in_rest'      => true,
		// 'rest_base'         => 'updates',
		'rest_base'         => 'resource_project_name',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
	) );

}
add_action( 'init', 'resource_project_type_init' );

/**
 * Sets the post updated messages for the `Project` taxonomy.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `Project` taxonomy.
 */
function resource_project_type_updated_messages( $messages ) {

	$messages['resource_project_type'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => __( 'Project added.', 'kairos' ),
		2 => __( 'Project deleted.', 'kairos' ),
		3 => __( 'Project updated.', 'kairos' ),
		4 => __( 'Project not added.', 'kairos' ),
		5 => __( 'Project not updated.', 'kairos' ),
		6 => __( 'Projects deleted.', 'kairos' ),
	);

	return $messages;
}
add_filter( 'term_updated_messages', 'resource_project_type_updated_messages' );
