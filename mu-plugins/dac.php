<?php
/**
 * Plugin Name: Design Action plugins
 * Description: A collection of must-use plugins
 * Author: Design Action Collective
*/

/*
 * Load all mu-plugins
*/

/* Custom Functions */
require_once(dirname(__FILE__) . '/dac/functions/dac_breadcrumbs.php');
require_once(dirname(__FILE__) . '/dac/functions/bootstrap_pagination.php');

/* Custom sections of Customizer */
require_once(dirname(__FILE__) . '/dac/customizer/social-media-and-external-links.php');
require_once(dirname(__FILE__) . '/dac/customizer/site-logo.php');

/* Custom post types */
require_once(dirname(__FILE__) . '/dac/post-types/jobs.php');
require_once(dirname(__FILE__) . '/dac/post-types/staff.php');
require_once(dirname(__FILE__) . '/dac/post-types/campaigns.php');
require_once(dirname(__FILE__) . '/dac/post-types/media-coverage.php');
require_once(dirname(__FILE__) . '/dac/post-types/press-releases.php');
require_once(dirname(__FILE__) . '/dac/post-types/op-eds.php');
require_once(dirname(__FILE__) . '/dac/post-types/publication.php');
require_once(dirname(__FILE__) . '/dac/post-types/network.php');
require_once(dirname(__FILE__) . '/dac/post-types/resources_cpt.php');

/* Custom taxonomies */
require_once(dirname(__FILE__) . '/dac/tax/resource-project-name.php');
require_once(dirname(__FILE__) . '/dac/tax/resource-type.php');
